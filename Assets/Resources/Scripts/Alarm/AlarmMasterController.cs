﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlarmMasterController : MonoBehaviour
{
    public GameObject[] alarmModules;

    private void Start()
    {
        alarmModules = GameObject.FindGameObjectsWithTag("Alarm Module");
    }

    public GameObject GetNearestAlarm(Vector3 targetPos)
    {
        if (alarmModules.Length == 0) return null;

        int targetAlarmModuleIndex = -1;
        float newClosestDist = float.MaxValue;
        for(int i = 0; i < alarmModules.Length; i++)
        {
            if(Vector3.Distance(targetPos, alarmModules[i].transform.position) < newClosestDist)
            {
                targetAlarmModuleIndex = i;
                 newClosestDist = Vector3.Distance(targetPos, alarmModules[i].transform.position);
            }
        }

        return alarmModules[targetAlarmModuleIndex];
    }
}
