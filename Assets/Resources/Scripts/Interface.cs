﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IEquipment
{
    void RecieveHeldClickTime(float deltaTime);
    bool IsInUse();
    bool IsCompletelyHolstered();

    void PlayAnimation(EquipmentAnimationTrigger trigger);

    void HoldingButton();
    void ReleaseHold();
    void UseRightAway();

    bool GetToggle();
    void ToggleEquipment();

    CurrentAnimationState GetCurrentAnimationState();
    bool HasFinishedFiring();
    bool HasSuccesfullyFired();
    void ResetExternalTriggers();
}

public enum EquipmentAnimationTrigger { unholster, holster, hold, swing, retract, quickSwing};

public enum CurrentAnimationState { Idle, Casting, Firing, Retracting, Holstering, Unholstering }

public interface ISpells
{
    int GetId();
    void OnHit();
    void OnCreate(int id);
}