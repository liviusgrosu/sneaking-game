﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightNode : MonoBehaviour
{
    private RaycastHit ray;
    private int layerMask = 1 << 8 | 1 << 9; // Only consider checking for any players or enviroment props 
    private Vector3 dirToPlayer;
    public float distToPlayer;

    private Light lightObj;
    private SphereCollider col;

    public float lightIntensityMax = 3.5f;
    private float lightExposureRatio = 0.0f;
    private float lightIntensityRatio = 0.0f;

    public int currLightPlayerVisibility;
    private bool recentPlayerExposure = false;

    private float angleOfPlayer;

    private void Start() {
        lightObj = GetComponent<Light>();
        col = GetComponent<SphereCollider>();
    }

    private void Update() {
        col.radius = lightObj.range;
    }

    private void OnTriggerEnter(Collider other) {
        if(other.tag == "Player") {
            //Add this light to list of lights affecting the player
            other.gameObject.GetComponent<PlayerDetection>().AddLightToList(this.transform);
        }
    }

    private void OnTriggerStay(Collider other) {
        if (other.tag == "Player") {
            dirToPlayer = other.transform.position - transform.position;

            //If this is a spot light, make sure that the player is within the spot lights angle
            if(lightObj.type == LightType.Spot) {
                angleOfPlayer = Vector3.Angle(dirToPlayer, transform.forward);

                if (angleOfPlayer > lightObj.spotAngle * 0.5f) {
                    //If the player recently left the spot light then its registered the player as not visible
                    //This was created so that exiting the spot light makes the player not visible but also to handle exiting a light and entering another
                    //If this wasnt here then it would constantly set the players visibility to 0 when they're actually visible from another light
                    if (recentPlayerExposure) {
                        currLightPlayerVisibility = 0;
                        recentPlayerExposure = false;
                    }
                    return;
                }
            }

            //Check the raycast from the light to the player
            if (Physics.Raycast(transform.position, dirToPlayer, out ray, dirToPlayer.magnitude, layerMask)) {
                if (ray.collider.tag == "Player") {
                    //Calculate the visibility by taking in variables such as distance from the light and light intensity 
                    distToPlayer = Vector3.Distance(ray.point, transform.position);

                    lightExposureRatio = 1.0f - (distToPlayer / lightObj.range);
                    lightIntensityRatio = lightObj.intensity / lightIntensityMax;

                    currLightPlayerVisibility = (int)(lightExposureRatio * lightIntensityRatio * 100f);
                    //print("dist: " + distToPlayer + ", int:" + currLightPlayerVisibility);
                }
                else {
                    //If an object is obstructing the light hitting the player
                    currLightPlayerVisibility = 0;
                }
                recentPlayerExposure = true;
            }
        }
    }

    private void OnTriggerExit(Collider other) {
        //Player exiting the light:
        //Set visibility to 0 
        //Remove the light the list of lights affecting the player
        if (other.tag == "Player") {
            recentPlayerExposure = false;
            currLightPlayerVisibility = 0;
            other.gameObject.GetComponent<PlayerDetection>().RemoveLightFromList(this.transform);
        }
    }
}
