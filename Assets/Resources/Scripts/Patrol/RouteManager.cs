﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RouteManager : MonoBehaviour
{
    [System.Serializable]
    public struct Route {
        public string attende;
        public Transform currentRoute;
        public Transform nextRoute;
        public int routeCycleMax;
    };
    public Route[] routeEntries;

    private void Start() {
        RecieveNewOrders(Enumerable.Range(0, routeEntries.Length).ToArray());
    }

    //Gets the given ai next duty 
    //Variables are passed by reference
    //Gives the player the current route, its next route, and the max cycles for the current route
    public void CheckNextDuty(string attendeName, out Transform currRoute, out Transform nextRoute, out int maxCycles) {
        foreach(Route routeEntry in routeEntries) {
            if (attendeName == routeEntry.attende) {
                currRoute = routeEntry.currentRoute;
                nextRoute = routeEntry.nextRoute;
                maxCycles = routeEntry.routeCycleMax;

                return;
            } 
        }
        currRoute = null;
        nextRoute = null;
        maxCycles = -1;
    }

    //Find the guard that the current guard needs to switch with
    public Transform FindNextRouteEntity(Transform nextRoute) {
        foreach(Route routeEntry in routeEntries) {
            if(nextRoute == routeEntry.currentRoute) {
                return GameObject.Find(routeEntry.attende).transform;
            }
        }
        return null;
    }

    //Looks for both route attendes index in the list and swap them
    public void ChangeRouteAttende(string relievingGuardName, Transform currRoute, Transform nextRoute) {

        int relievedGuardIndex = -1;
        int relievingGuardIndex = -1;
        string relievedGuardName = "";

        for (int i = 0; i < routeEntries.Length; i++) {
            if (currRoute == routeEntries[i].currentRoute) relievingGuardIndex = i;
            if (nextRoute == routeEntries[i].currentRoute) { relievedGuardIndex = i; relievedGuardName = routeEntries[i].attende; }
        }

        routeEntries[relievingGuardIndex].attende = relievedGuardName;
        routeEntries[relievedGuardIndex].attende = relievingGuardName;

        //Update those guards duties
        RecieveNewOrders(new int[] {relievingGuardIndex, relievedGuardIndex});
    }

    //Update a list of guards duties 
    void RecieveNewOrders(int[] guards) {
        foreach(int index in guards) {
            if (GameObject.Find(routeEntries[index].attende) != null && GameObject.Find(routeEntries[index].attende).activeSelf) {
                GameObject.Find(routeEntries[index].attende).GetComponent<EnemyAIPatrol>().RecieveOrder();
            }
        }
    }
}
