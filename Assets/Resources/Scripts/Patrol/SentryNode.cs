﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SentryNode : MonoBehaviour
{
    public float timeBetweenEachAngle = 0.0f;
    private Transform[] sentryPoints;

    private void Start() {
       sentryPoints = Array.FindAll(GetComponentsInChildren<Transform>(), child => child != this.transform);
    }

    public Vector3 GetSentryPos(int nodeIndex) {
        return sentryPoints[nodeIndex].position;
    }

    public int GetSentrySize() {
        return sentryPoints.Length;
    }

    public float GetWaitTimes() {
        return timeBetweenEachAngle;
    }
}
