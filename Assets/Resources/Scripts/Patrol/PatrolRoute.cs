﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PatrolRoute : MonoBehaviour
{
    public Transform[] patrolNodes;

    private void Awake() {
        patrolNodes = new Transform[transform.childCount];
        for (int i = 0; i < transform.childCount; ++i) {
            patrolNodes[i] = transform.GetChild(i);
        }
    }

    public Vector3 GetNextNodePos(int nodeIndex) {
        return patrolNodes[nodeIndex].position;
    }

    public int PathSize() {
        return patrolNodes.Length;
    }

    public Transform GetNode(int nodeIndex) {
        return patrolNodes[nodeIndex];
    }
}
