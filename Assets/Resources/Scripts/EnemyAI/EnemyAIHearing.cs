﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyAIHearing : MonoBehaviour
{
    private NavMeshAgent earAgent;
    private EnemyAIAwarness aiSenses;
    public int enemyPlayerHearingLevel = 0;
    private int enemyHearingOffset = 0;
    private float playerSoundLevel = 0;

    private LayerMask ignoreLayerMask = 1 << 10;
    private RaycastHit intermediateRay;
    public float passThroughSoundTolerance = 2f;

    private void Start() {
        earAgent = transform.parent.GetComponent<NavMeshAgent>();
        aiSenses = transform.parent.GetComponent<EnemyAIAwarness>();
    }

    private void OnTriggerEnter(Collider other) {
        if(other.tag == "Player Sound Sphere") {
            if (CalculatePathLength(other.transform.position) <= other.GetComponent<SphereCollider>().radius + 2f)
                aiSenses.UpdatePlayerSoundLevel(other.GetComponent<PlayerSoundSphere>().triggerLevels);
        }
    }

    float CalculatePathLength(Vector3 targetPosition) {
        NavMeshPath soundPath = new NavMeshPath();

        if (earAgent.enabled)
            earAgent.CalculatePath(targetPosition, soundPath);

        Vector3[] allWayPoints = new Vector3[soundPath.corners.Length + 2];

        allWayPoints[0] = transform.position;
        allWayPoints[allWayPoints.Length - 1] = targetPosition;

        for(int i = 0; i < soundPath.corners.Length; i++) {
            allWayPoints[i + 1] = soundPath.corners[i];
        }

        float pathLength = 0f;

        for(int i = 0; i < allWayPoints.Length - 1; i++) {
            pathLength += Vector3.Distance(allWayPoints[i], allWayPoints[i + 1]);
            pathLength += CalculateForIntermediateObjects(allWayPoints[i], allWayPoints[i + 1]);
        }

        DrawPath(allWayPoints);

        return pathLength;
    }

    float CalculateForIntermediateObjects(Vector3 corner1, Vector3 corner2) {
        if(Physics.Raycast(corner1, corner2 - corner1, out intermediateRay, Vector3.Distance(corner1, corner2),ignoreLayerMask.value)) {
            return passThroughSoundTolerance;
        }
        return 0f;
    }

    void DrawPath(Vector3[] allWayPoints) {
        for(int i = 0; i < allWayPoints.Length - 1; i++) {
            Debug.DrawRay(allWayPoints[i], allWayPoints[i + 1] - allWayPoints[i], Color.red);
        }
    }
}
