﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyAIAlarmTrigger : MonoBehaviour
{
    private AlarmMasterController alarmMaster;
    private NavMeshAgent agent;

    private GameObject nearestAlarm;
    private Vector3 lastKnownPlayerPos;

    public float maxDistToAlarm = 20f;
    private bool goingToAlarm, usingAlarm;
    
    // Start is called before the first frame update
    void Start()
    {
        alarmMaster = GameObject.Find("Alarm Master Controller").GetComponent<AlarmMasterController>();
        agent = GetComponent<NavMeshAgent>();
    }

    // Update is called once per frame
    void Update()
    {
        if (goingToAlarm)
        {
            if (Vector3.Distance(nearestAlarm.transform.position, transform.position) < 2.5f)
            {
                if (!usingAlarm)
                {
                    StartCoroutine(TempTakeTimeToSoundAlarm());
                }
            }
        }
    }
    //Check if an alarm exists or not
    //If so then send TRUE otherwise send FALSE
    public bool IsAlarmNear()
    {

        nearestAlarm = alarmMaster.GetNearestAlarm(transform.position);

        if (nearestAlarm != null && Vector3.Distance(transform.position, nearestAlarm.transform.position) <= maxDistToAlarm)
            return true;
        else
            return false;
    }

    public void RememberLastKnownPlayerPos(Vector3 pos)
    {
        lastKnownPlayerPos = pos;
    }

    public void SetOffAlarm()
    {
        goingToAlarm = true;
        MoveToNextPos(nearestAlarm.transform.position);
    }

    public bool IsGoingToAlarm()
    {
        return goingToAlarm;
    }

    void MoveToNextPos(Vector3 point)
    {
        agent.SetDestination(point);
    }
    
    //Mimic the effect of the animation raising the alarm
    IEnumerator TempTakeTimeToSoundAlarm()
    {
        usingAlarm = true;
        agent.isStopped = true;

        yield return new WaitForSeconds(2f);

        nearestAlarm.GetComponent<AlarmController>().SoundAlarm();

        nearestAlarm = null;
        goingToAlarm = false;
        usingAlarm = false;
        agent.isStopped = false;

        MoveToNextPos(lastKnownPlayerPos);

    }
}
