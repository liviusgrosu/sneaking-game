﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyAIFightingController : MonoBehaviour
{
    private EnemyAIAwarness aiAwarness;
    private NavMeshAgent agent;

    private float distToEngage = 1.3f;

    private bool closeEnoughToPlayer;

    private float tempAttackingWindow = 1.0f;
    private float tempCurrAttackingTime = 0f;
    private PlayerStats tempPlayerStats;

    private float tempDamageOutput = 20f;
         
    // Start is called before the first frame update
    void Start()
    {
        aiAwarness = GetComponent<EnemyAIAwarness>();
        agent = GetComponent<NavMeshAgent>();
        tempPlayerStats = GameObject.Find("Player Stats Controller").GetComponent<PlayerStats>();
    }

    // Update is called once per frame
    void Update()
    {
        if (aiAwarness.GetCurrentAIState() == EnemyAIAwarness.State.Alert)
        {
            if (aiAwarness.GetDistanceFromPlayer() <= distToEngage) closeEnoughToPlayer = true;
            else closeEnoughToPlayer = false;

            if (closeEnoughToPlayer)
            {
                //if (!agent.isStopped)
                //{
                //    agent.isStopped = true;
                //}

                tempCurrAttackingTime += Time.deltaTime;
                if(tempCurrAttackingTime >= tempAttackingWindow)
                {
                    tempCurrAttackingTime = 0f;
                    //Decrease the players health
                    tempPlayerStats.ChangeHealthByIncrememnt(-tempDamageOutput);
                }
            }
            else
            {
                //if (agent.isStopped)
                //{
                //    agent.isStopped = false;
                //}
            }
        }
        //Check distance from the player
        //If its close enough then engage
        // - Pause the AI movement 
        //When the player is not close enough then resume the AI 

        //Deal with the player on ground
        //Deal with the player when not reachable
    }
}
