﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyAIAwarness : MonoBehaviour {


    private EnemyAIPatrol aiPatrol;
    public EnemySoundController soundController;
    private EnemyAIAlarmTrigger alarmTrigger;
    public NavMeshAgent agent;
    private Renderer rend;

    private float soundCautionLevel, soundInvestigationLevel, soundAlertLevel;
    private float sightCautionLevel, sightInvestigationLevel, sightAlertLevel;

    private float currSoundCautionTimer = 0.0f;
    private float currSightCautionTimer = 0.0f;
    private float currInvestToPatrolTimer = 0.0f;
    private float currInvestToNotMoveTimer = 0.0f;
    private float currInvestInitialPause = 0.0f;
    private float currAlertToCoolDownTimer = 0.0f;
    private float currCoolDownToTaskingTimer = 0.0f;
    private float currCoolDownToInvestTimer = 0.0f;
    private float currRecentlyInvestigatedTimer = 0.0f;
    private float currRecentlyAlertedTimer = 0.0f;

    [Header("State Transition Timers")]
    public float maxCautionTimer = 8.0f;
    public float maxInvestToPatrolTimer = 4.0f;
    public float maxInvestToNotMoveTimer = 5.0f;
    public float maxInvestInitialPause = 1.0f;
    public float maxAlertToCoolDownTImer = 0.0f;
    public float maxCoolDownToTaskingTimer = 0.0f;
    public float maxCoolDownToInvestTimer = 0.0f;

    [Space(5)]
    [Header("Recently Investigated Timers")]
    public float maxRecentlyInvestigatedTimer = 10.0f;
    public float maxRecentlyAlertedTimer = 0.0f;

    [Space(5)]
    [Header("Recently Investigated Timer Modifiers")]
    private float cautiousTimerMod = 1.0f;
    public float cautionToInvestMult = 2.5f;
    public float cautiousTimerModMult = 0.75f;

    private int playerSoundOccuranceToMoveInInvest = 0;
    private int playerSightOccuranceToMoveInInvest = 0;
    private bool movingToInvestigate = false;
    private int investLookPointsInc = 0;
    private Vector3[] investLookPoints;
   
    private bool recentlyAlerted, recentlyInvestigated;

    private Transform playerObj;
    private Vector3 lastPlayerPos;
    private Vector3 lastPlayerPotentialPos;
    private float distToPlayer = 0.0f;
    public float editorDistToPlayer;
    private bool triggerNewPlayerSoundInfo, triggerNewPlayerLightInfo;

    public enum State {
        Tasking,
        Cautious,
        Investigative,
        Alert,
        Cooldown
    };

    private State currentState = State.Tasking;
    private bool initTasking, initCautious, initInvestigative, initAlert, initCooldown;

    [Space(5)]
    [Header("State Materials")]
    public Material taskMat;
    public Material cautMat;
    public Material investMat;
    public Material alertMat;
    public Material coolDownMat;

    [Space(5)]
    [Header("AI Speeds")]
    public float normalSpeed = 1.0f;
    public float slowSpeed = 0.5f;
    public float fastSpeed = 1.75f;

    private bool lookAtTarget = false;
    private bool allowedToMove = false;
    private bool goingToTheNearestAlarm = false;

    [Space(5)]
    [Header("Rotation Speed")]
    public float rotationSpeed = 1.0f;

    [Space(5)]
    [Header("Misc")]
    public bool drawSightDistanceTriggers;

    private void Start() {
        agent = GetComponent<NavMeshAgent>();
        aiPatrol = GetComponent<EnemyAIPatrol>();
        rend = GetComponent<Renderer>();
        alarmTrigger = GetComponent<EnemyAIAlarmTrigger>();

        playerObj = GameObject.Find("Player").transform;
        agent.speed = normalSpeed;

        soundCautionLevel = soundInvestigationLevel = soundAlertLevel = 0f;
        sightCautionLevel = sightInvestigationLevel = sightAlertLevel = 0f;

        //We begin in the tasking state
        initTasking = true;
    }

    private void FixedUpdate() {

        editorDistToPlayer = Vector3.Distance(transform.position, playerObj.position);

        //Calculate the AI state
        if (currentState == State.Tasking)          CalculateTaskingState();
        if (currentState == State.Cautious)         CalculateCautiousState();
        if (currentState == State.Investigative)    CalculateInvestigativeState();
        if (currentState == State.Alert)            CalculateAlertState();
        if (currentState == State.Cooldown)         CalculateCoolDownState();

        //Whenever new player info reaches the AI, update the states with it and it'll react to that new info
        if (triggerNewPlayerSoundInfo) triggerNewPlayerSoundInfo = false;

        if (triggerNewPlayerLightInfo) triggerNewPlayerLightInfo = false;

        //If the AI recently investigated then the tolerance to switch back to that state becomes shorter thus switching to it faster
        cautiousTimerMod = (recentlyInvestigated || recentlyAlerted) ? cautiousTimerModMult : 1.0f;
    }

    public void UpdatePlayerSoundLevel(float[] triggerLevels) {

        print("hearing new sounds");

        //Get the cautious, investigation, and alert distances
        soundCautionLevel = triggerLevels[0];
        soundInvestigationLevel = triggerLevels[1];
        soundAlertLevel = triggerLevels[2];

        //If any of these are new info, tell the AI this new info
        if (soundCautionLevel != 0 || soundInvestigationLevel != 0 || soundAlertLevel != 0) {
            triggerNewPlayerSoundInfo = true;
        }

        //Get the distance from the AI to the player
        distToPlayer = Vector3.Distance(transform.position, playerObj.transform.position);
    }

    //REDO
    public void UpdatePlayerVisibilityLevel(float[] triggerLevels) {
        //Get the cautious, investigation, and alert distances

        sightCautionLevel = Mathf.Abs(triggerLevels[0]) + Mathf.Abs(triggerLevels[1]) + triggerLevels[2];
        sightInvestigationLevel = Mathf.Abs(triggerLevels[1]) + triggerLevels[2] ;
        sightAlertLevel = triggerLevels[2];

        CalculateSightDistanceTriggers();

        if (sightCautionLevel != 0 || sightInvestigationLevel != 0 || sightAlertLevel != 0) {
            triggerNewPlayerLightInfo = true;
        }

        //Get the distance from the AI to the player
        distToPlayer = Vector3.Distance(transform.position, playerObj.transform.position);
    }

    void CalculateSightDistanceTriggers() {
        if (sightCautionLevel != 0 && sightInvestigationLevel != 0 && sightAlertLevel != 0) 
        {
            Vector3 playerDir   = (playerObj.position - transform.position).normalized;
            Vector3 alertDir    = playerDir * sightAlertLevel;
            Vector3 investDir   = playerDir * sightInvestigationLevel;
            Vector3 cautDir     = playerDir * sightCautionLevel;

            if (drawSightDistanceTriggers) {
                Debug.DrawRay(transform.position, alertDir, Color.red);
                Debug.DrawRay(transform.position + alertDir, investDir, Color.blue);
                Debug.DrawRay(transform.position + investDir + alertDir, cautDir, Color.yellow);
            }

            sightAlertLevel = Vector3.Distance(transform.position, transform.position + alertDir);
            sightInvestigationLevel = Vector3.Distance(transform.position, transform.position + alertDir + investDir);
            sightCautionLevel = Vector3.Distance(transform.position, transform.position + alertDir + investDir + cautDir);
        }
    }

    //-----------------------
    //TASKING
    //-----------------------
    void CalculateTaskingState() {
        //One timer calcualte when entering this state
        if (initTasking) {
            //Set back to the normal speed
            agent.speed = normalSpeed;
            rend.material = taskMat;

            //If the enemy was patrolling then go back to it
            aiPatrol.ContinuePatrol();

            initTasking = false;
        }

        //If the AI tasks for a bit then the tolernace to transitioning to the investigation state becomes normal normal 
        if(recentlyInvestigated || recentlyAlerted) {
            currRecentlyInvestigatedTimer -= Time.deltaTime;
            currRecentlyAlertedTimer -= Time.deltaTime;
        }
        //If the timers hit 0 then remove this tolerance modifier
        if (currRecentlyInvestigatedTimer <= 0.0f) recentlyInvestigated = false;
        if (currRecentlyAlertedTimer <= 0.0f) recentlyAlerted = false;


        //---SOUND CODE---
        //Player info updated to the AI
        if (triggerNewPlayerSoundInfo) {
            //If the player is within the cautious distance, then transition to that state
            if (distToPlayer <= soundCautionLevel && distToPlayer > soundInvestigationLevel) {
                currentState = State.Cautious;
                initCautious = true;
                soundController.PlaySound(currentState, EnemySoundController.SenseTrigger.hearing);
                return;
            }
            //If the player is within the investigation distance, then transition to that state
            if (distToPlayer <= soundInvestigationLevel && distToPlayer > soundAlertLevel) {
                currentState = State.Investigative;
                initInvestigative = true;
                soundController.PlaySound(currentState, EnemySoundController.SenseTrigger.hearing);
                return;
            }
            //If the player is within the alert distance, then transition to that state
            if (distToPlayer <= soundAlertLevel) {
                currentState = State.Alert;
                initAlert = true;
                soundController.PlaySound(currentState);
                return;
            }
        }

        //---SIGHT CODE---
        if (triggerNewPlayerLightInfo) {
            //If the player is within the cautious distance, then transition to that state
            if (distToPlayer <= sightCautionLevel && distToPlayer > sightInvestigationLevel) {
                currentState = State.Cautious;
                initCautious = true;
                soundController.PlaySound(currentState, EnemySoundController.SenseTrigger.seeing);
                return;
            }
            
            //If the player is within the investigation distance, then transition to that state
            if (distToPlayer <= sightInvestigationLevel && distToPlayer > sightAlertLevel) {
                currentState = State.Investigative;
                initInvestigative = true;
                soundController.PlaySound(currentState, EnemySoundController.SenseTrigger.seeing);
                return;
            }
            
            //If the player is within the alert distance, then transition to that state
            if (distToPlayer <= sightAlertLevel) {
                currentState = State.Alert;
                initAlert = true;
                soundController.PlaySound(currentState);
                return;
            }
            
        }
    }

    //-----------------------
    //CAUTIOUS
    //-----------------------
    void CalculateCautiousState() {
        //One timer calcualte when entering this state
        if (initCautious) {
            currSoundCautionTimer = cautionToInvestMult;
            currSightCautionTimer = cautionToInvestMult;
            rend.material = cautMat;

            initCautious = false;
        }

        //---SOUND CODE---
        //If the players prescence is know to the AI frequently then transition to the investigation state
        if(currSoundCautionTimer >= maxCautionTimer * cautiousTimerMod) {
            currentState = State.Investigative;
            initInvestigative = true;
            soundController.PlaySound(currentState, EnemySoundController.SenseTrigger.hearing);
            return;
        }
        //Count down a timer that determines wether or not to the AI transitions to tasking or investigation state
        //If its equal or below 0 then transition back to the tasking state
        //If its equal or above the transition timer then transition to the investigation
        else if(currSoundCautionTimer > 0.0f && currSoundCautionTimer < maxCautionTimer * cautiousTimerMod) {
            currSoundCautionTimer -= Time.deltaTime;
        }
        else {
            currentState = State.Tasking;
            initTasking = true;
            soundController.PlayTransitionSound(false);
            return;
        }

        //---SIGHT CODE---
        if (currSightCautionTimer >= (maxCautionTimer * 20f) * cautiousTimerMod) {
            currentState = State.Investigative;
            initInvestigative = true;
            soundController.PlaySound(currentState, EnemySoundController.SenseTrigger.seeing);
            return;
        }
        //Count down a timer that determines wether or not to the AI transitions to tasking or investigation state
        //If its equal or below 0 then transition back to the tasking state
        //If its equal or above the transition timer then transition to the investigation
        else if (currSightCautionTimer > 0.0f && currSightCautionTimer < (maxCautionTimer * 20f) * cautiousTimerMod) {
            currSightCautionTimer -= Time.deltaTime;
        }
        else {
            currentState = State.Tasking;
            initTasking = true;
            soundController.PlayTransitionSound(false);
            return;
        }

        //---SOUND CODE---
        //Player info updated to the AI
        if (triggerNewPlayerSoundInfo) {

            //If the player is within the cautious distance, then update that timer above
            if (distToPlayer <= soundCautionLevel && distToPlayer > soundInvestigationLevel) {
                currSoundCautionTimer += cautionToInvestMult;
            }
            //If the player is within the investigation distance, then transition to that state
            if (distToPlayer <= soundInvestigationLevel && distToPlayer > soundAlertLevel) {
                currentState = State.Investigative;
                initInvestigative = true;
                soundController.PlaySound(currentState, EnemySoundController.SenseTrigger.hearing);
                return;
            }
            //If the player is within the alert distance, then transition to that state
            if (distToPlayer <= soundAlertLevel) {
                currentState = State.Alert;
                initAlert = true;
                soundController.PlaySound(currentState);
                return;
            }
        }
        //---SIGHT CODE---
        //Player info updated to the AI
        if (triggerNewPlayerLightInfo) {

            //If the player is within the cautious distance, then update that timer above
            if (distToPlayer <= sightCautionLevel && distToPlayer > sightInvestigationLevel) {
                currSightCautionTimer += cautionToInvestMult;
            }
            //If the player is within the investigation distance, then transition to that state
            if (distToPlayer <= sightInvestigationLevel && distToPlayer > sightAlertLevel) {
                currentState = State.Investigative;
                initInvestigative = true;
                soundController.PlaySound(currentState, EnemySoundController.SenseTrigger.seeing);
                return;
            }
            //If the player is within the alert distance, then transition to that state
            if (distToPlayer <= sightAlertLevel) {
                currentState = State.Alert;
                initAlert = true;
                soundController.PlaySound(currentState);
                return;
            }
        }
    }

    //-----------------------
    //INVESTIGATIVE
    //-----------------------
    void CalculateInvestigativeState() {
        //One timer calcualte when entering this state
        if (initInvestigative) {

            //Slow down the speed
            agent.speed = slowSpeed;

            //Get the last players position
            UpdateLastPlayerPosition();

            //Timer that determine if the AI should go into the patrol state
            currInvestToPatrolTimer = maxInvestToPatrolTimer;
            //Timer that determine if the AI should start moving to the last players position
            currInvestToNotMoveTimer = maxInvestToNotMoveTimer;

            //Time that pauses the AI initially when entering this state to immitate shock and confusion
            if (!recentlyAlerted && !recentlyInvestigated)
                currInvestInitialPause = maxInvestInitialPause;

            //Number of updated player info
            //If this reaches a certain number then the AI starts moving to investigate
            playerSoundOccuranceToMoveInInvest = 0;
            playerSightOccuranceToMoveInInvest = 0;

            //Remove after testing
            rend.material = investMat;

            //AI begins to look at the laster player position
            lookAtTarget = true;
            //The AI doesn't move to the last players position yet
            movingToInvestigate = false;

            //Points that the AI starts looking towards once its reached the last known player pos
            ResetQuickTurns();

            //AI stops its track when entering this state
            //This is needed so that it doesn't continue going to a last known player position & last know potential player position
            agent.isStopped = true;
            allowedToMove = false;

            //If the enemy is patrolling then stop pause it
            if (!aiPatrol.HasPausedPatrol()) aiPatrol.PausePatrol();

            initInvestigative = false;
        }
        //Pause AI movement for a second but allow for rotation
        //If the enemy has been already alerted recently then no need to pause
        if(currInvestInitialPause > 0f)
        {
            currInvestInitialPause -= Time.deltaTime;
        }
        else
        {
            allowedToMove = true;
        }


        //Look at the last player position when entering this state
        if (lookAtTarget) {
            //Stop looking at the target once the AI reached its looking target
            if (RotateTowardsTarget(lastPlayerPos)) lookAtTarget = false;
        }
        else {
            //Timer that counts down which determines if the AI goes back to tasking or starts moving to the last known player position
            if (!movingToInvestigate) currInvestToNotMoveTimer -= Time.deltaTime;
        }

        //If this timer reaches 0 then go back to tasking
        if(currInvestToNotMoveTimer <= 0f) {
            currentState = State.Tasking;
            initTasking = true;
            soundController.PlayTransitionSound(recentlyAlerted);
            return;
        }

        //If the AI reaches the last known player pos then count down a timer 
        if (Vector3.Distance(transform.position, lastPlayerPos) < 0.1f) {
            currInvestToPatrolTimer -= Time.deltaTime;
            //AI looks right, left, and forward once it reaches this position
            CalculateQuickTurns();
        }

        //If this timer reaches 0 then go back to tasking
        if (currInvestToPatrolTimer <= 0.0f) {
            recentlyInvestigated = true;
            //Reset this timer that determines the tolerance to transition back to this state
            currRecentlyInvestigatedTimer = maxRecentlyInvestigatedTimer;
            currentState = State.Tasking;
            initTasking = true;
            soundController.PlayTransitionSound(recentlyAlerted);
            return;
        }

        //---SOUND CODE---
        //Player info updated to the AI
        if (triggerNewPlayerSoundInfo && allowedToMove) {
            //If the player is within the cautious distance...
            if (distToPlayer <= soundCautionLevel && distToPlayer > soundInvestigationLevel) {
                //Number of player occurances increases 
                playerSoundOccuranceToMoveInInvest++;
                //Timer that determines to transition back to tasking resets
                currInvestToNotMoveTimer = maxInvestToNotMoveTimer;
                //Once the amount of player occurances reaches 3 then the AI starts moving to the last known player pos
                if (playerSoundOccuranceToMoveInInvest >= 3) {
                    currInvestToPatrolTimer = maxInvestToPatrolTimer;
                    UpdateLastPlayerPosition();
                    MoveToPoint(lastPlayerPos);
                    movingToInvestigate = true;
                    agent.isStopped = false;
                }
                return;
            }
            
            //If the player is within the investigation distance then immediatly the AI moves to the last known player pos
            if (distToPlayer <= soundInvestigationLevel && distToPlayer > soundAlertLevel) {
                currInvestToPatrolTimer = maxInvestToPatrolTimer;
                UpdateLastPlayerPosition();
                MoveToPoint(lastPlayerPos);
                movingToInvestigate = true;
                agent.isStopped = false;
                return;
            }

            //If the player is within the alert distance, then transition to that state
            if (distToPlayer <= soundAlertLevel) {
                currentState = State.Alert;
                initAlert = true;
                soundController.PlaySound(currentState);
                return;
            }
        }

        //---SIGHT CODE---
        if (triggerNewPlayerLightInfo && allowedToMove) {
            //If the player is within the cautious distance...
            if (distToPlayer <= sightCautionLevel && distToPlayer > sightInvestigationLevel) {
                //Number of player occurances increases 
                playerSightOccuranceToMoveInInvest++;
                //Timer that determines to transition back to tasking resets
                currInvestToNotMoveTimer = maxInvestToNotMoveTimer;
                //Once the amount of player occurances reaches 3 then the AI starts moving to the last known player pos
                if (playerSightOccuranceToMoveInInvest >= 200) {
                    currInvestToPatrolTimer = maxInvestToPatrolTimer;
                    UpdateLastPlayerPosition();
                    MoveToPoint(lastPlayerPos);
                    movingToInvestigate = true;
                    agent.isStopped = false;
                }
                return;
            }

            //If the player is within the investigation distance then immediatly the AI moves to the last known player pos
            if (distToPlayer <= sightInvestigationLevel && distToPlayer > sightAlertLevel) {
                currInvestToPatrolTimer = maxInvestToPatrolTimer;
                UpdateLastPlayerPosition();
                MoveToPoint(lastPlayerPos);
                movingToInvestigate = true;
                agent.isStopped = false;
                return;
            }

            //If the player is within the alert distance, then transition to that state
            if (distToPlayer <= sightAlertLevel) {
                currentState = State.Alert;
                initAlert = true;
                soundController.PlaySound(currentState);
                return;
            }
        }
    }

    //-----------------------
    //ALERT
    //-----------------------
    void CalculateAlertState() {
        //One timer calcualte when entering this state
        if (initAlert) {
            //Increase the speed
            agent.speed = fastSpeed;
            rend.material = alertMat;
            //Timer that determines wether the AI goes to the cooldown stage
            currAlertToCoolDownTimer = maxAlertToCoolDownTImer;

            //Points that the AI starts looking towards once its reached the last known player pos
            ResetQuickTurns();

            //AI has recently been alerted 
            recentlyAlerted = true;
            currRecentlyAlertedTimer = maxRecentlyAlertedTimer;

            //If the enemy is patrolling then stop pause it
            if (!aiPatrol.HasPausedPatrol()) aiPatrol.PausePatrol();

            //Try to sound off the alarm
            if(alarmTrigger.IsAlarmNear())
            {
                agent.isStopped = false;
                goingToTheNearestAlarm = true;
                aiPatrol.PausePatrol();
                alarmTrigger.SetOffAlarm();
                alarmTrigger.RememberLastKnownPlayerPos(lastPlayerPos);
            }

            initAlert = false;
        }

        if (alarmTrigger.IsGoingToAlarm())
            return;

        //Someway to go back to the last player position after setting off the alarm

        //Once the AI has caught up to the last known player position a timer counts down
        //Once then timer hits 0 or below, go into the cooldown stage
        if (Vector3.Distance(transform.position, lastPlayerPos) < 0.1f) {
            CalculateQuickTurns();

            currAlertToCoolDownTimer -= Time.deltaTime;

            if(currAlertToCoolDownTimer <= 0.0f) {
                currentState = State.Cooldown;
                initCooldown = true;
                soundController.PlaySound(currentState);
                return;
            }
        }

        //---SOUND CODE---
        //Player info updated to the AI
        if (triggerNewPlayerSoundInfo) {
            //If the player is within the cautious distance then move to that positon
            if (distToPlayer <= soundCautionLevel) {
                UpdateLastPlayerPosition();
                MoveToPoint(lastPlayerPos);
                currAlertToCoolDownTimer = maxAlertToCoolDownTImer;
                return;
            }
        }
        //---SIGHT CODE---
        if (triggerNewPlayerLightInfo) {
            //If the player is within the cautious distance then move to that positon
            if (distToPlayer <= sightCautionLevel) {
                UpdateLastPlayerPosition();
                MoveToPoint(lastPlayerPos);
                currAlertToCoolDownTimer = maxAlertToCoolDownTImer;
                return;
            }
        }
    }

    //-----------------------
    //COOLDOWN
    //-----------------------
    void CalculateCoolDownState() {
        //One timer calcualte when entering this state
        if (initCooldown) {

            rend.material = coolDownMat;

            //Move to the last known player position
            MoveToPoint(lastPlayerPotentialPos);

            currCoolDownToTaskingTimer = maxCoolDownToTaskingTimer;

            //Points that the AI starts looking towards once its reached the last known player pos
            ResetQuickTurns();

            if (!aiPatrol.HasPausedPatrol()) aiPatrol.PausePatrol();

            initCooldown = false;
        }

        //If the AI reaches the last known potential positon then decrease the timer below & do a quick turn procedure
        if(Vector3.Distance(transform.position, lastPlayerPotentialPos) < 0.1f) {
            CalculateQuickTurns();
            currCoolDownToTaskingTimer -= Time.deltaTime;
        }
        //If the timer reaches 0 then get back to tasking
        if(currCoolDownToTaskingTimer <= 0f) {
            currentState = State.Tasking;
            initTasking = true;
            soundController.PlayTransitionSound(true);
            return;
        }

        //Player info updated to the AI
        if (triggerNewPlayerSoundInfo) {
            //If the player is within the cautious distance then transition to the investigation state
            if (distToPlayer <= soundCautionLevel) {
                currentState = State.Investigative;
                initInvestigative = true;
                soundController.PlaySound(currentState, EnemySoundController.SenseTrigger.hearing);
                return;
            }
            //If the player is within the investigation distance then immediatly the AI moves to the last known player pos
            if (distToPlayer <= soundInvestigationLevel && distToPlayer > soundAlertLevel) {
                currentState = State.Alert;
                initAlert = true;
                soundController.PlaySound(currentState);
                return;
            }
        }
        //---------------------------------
        //TODO: Add sight trigger info too!
        //---------------------------------
    }

    //Update the last known player position and the last known potential player position
    void UpdateLastPlayerPosition() {
        lastPlayerPos = playerObj.position;
        StopCoroutine(CalculatePotentialPlayerLocation());
        StartCoroutine(CalculatePotentialPlayerLocation());
    }

    //Wait a bit and find the last known potential player position
    IEnumerator CalculatePotentialPlayerLocation() {
        yield return new WaitForSeconds(3.5f);
        lastPlayerPotentialPos = playerObj.position;
    }

    //Go to the next node in the route
    void MoveToPoint(Vector3 point) {
        agent.SetDestination(point);
    }

    //Rotate entity towards a point and once it reaches that point return true to signify that its done turning
    bool RotateTowardsTarget(Vector3 target) {
        Vector3 toTarget = target - transform.position;
        Quaternion targetRotation = Quaternion.LookRotation(toTarget);
        transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, rotationSpeed);
        if (RotationEquals(transform.rotation, targetRotation))
            return true;
        else
            return false;
    }

    //Check if the entity has rotated to the specified position
    bool RotationEquals(Quaternion r1, Quaternion r2) {
        float abs = Mathf.Abs(Quaternion.Dot(r1, r2));
        if (abs >= 0.99f)
            return true;
        return false;
    }

    //Reset the quick turns procedure variables 
    void ResetQuickTurns() {
        investLookPoints = null;
        investLookPointsInc = 0;
    }

    //AI will turn to 3 positons 
    //Right, Left, and forward
    void CalculateQuickTurns() {
        if (investLookPoints == null) {
            investLookPoints = new Vector3[3];
            investLookPoints[0] = transform.position + transform.right;
            investLookPoints[1] = transform.position - transform.right;
            investLookPoints[2] = transform.position + transform.forward;
        }

        if (investLookPointsInc < investLookPoints.Length && RotateTowardsTarget(investLookPoints[investLookPointsInc])) {
            investLookPointsInc++;
        }
    }

    //Get the distance from the AI to the player
    public float GetDistanceFromPlayer()
    {
        return distToPlayer;
    }

    //Get the current AI state
    public State GetCurrentAIState()
    {
        return currentState;
    }
}
