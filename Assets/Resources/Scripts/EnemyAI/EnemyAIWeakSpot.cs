﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAIWeakSpot : MonoBehaviour
{
    private EnemyAIStats enemyStats;

    private void Start()
    {
        enemyStats = transform.parent.GetComponent<EnemyAIStats>();
    }

    public void KnockOut()
    {
        enemyStats.KnockOut();
    }
}
