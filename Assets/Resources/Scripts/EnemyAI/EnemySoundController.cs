﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySoundController : MonoBehaviour
{
    private AudioSource audioSrc;

    //Transition Sounds
    [Header("Patrolling sounds")]
    public AudioClip[] patrolSounds;

    [Header("Caution Sounds")]
    public AudioClip[] cautionSightSounds;
    public AudioClip[] cautionHearingSounds;

    [Header("Investigation Sounds")]
    public AudioClip[] investigationSightSounds;
    public AudioClip[] investigationHearingSounds;


    [Header("Alert Sounds")]
    public AudioClip[] alertSounds;

    [Header("Cooldown Sounds")]
    public AudioClip[] cooldownSounds;

    [Header("Patrol Transition Sounds")]
    public AudioClip[] lightPatrolTransitionSounds;
    public AudioClip[] heavyPatrolTransitionSounds;

    [Header("Enemy Hit Sounds")]
    public AudioClip[] hitSounds;
    public AudioClip[] dyingSounds;
    public AudioClip[] knockedOutSounds;


    public enum SenseTrigger
    {
        hearing, 
        seeing
    };

    // Start is called before the first frame update
    void Start()
    {
        audioSrc = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void PlaySound(EnemyAIAwarness.State enemyState, SenseTrigger triggerType)
    {
        if(enemyState == EnemyAIAwarness.State.Cautious)
        {
            if(triggerType == SenseTrigger.hearing)         PlaySound(cautionHearingSounds);
            else if (triggerType == SenseTrigger.seeing)    PlaySound(cautionSightSounds);
        }

        else if (enemyState == EnemyAIAwarness.State.Investigative)
        {
            if (triggerType == SenseTrigger.hearing)        PlaySound(investigationHearingSounds);
            else if (triggerType == SenseTrigger.seeing)    PlaySound(investigationSightSounds);
        }
    }

    public void PlaySound(EnemyAIAwarness.State enemyState)
    {
        if (enemyState == EnemyAIAwarness.State.Alert)      PlaySound(alertSounds);
        else if (enemyState == EnemyAIAwarness.State.Cooldown)    PlaySound(cooldownSounds);
    }

    public void PlaySound(EnemyAIStats.AttackType attackType)
    {
        if (attackType == EnemyAIStats.AttackType.Regular)  PlaySound(hitSounds);
        if (attackType == EnemyAIStats.AttackType.Knockout) PlaySound(knockedOutSounds);
        if (attackType == EnemyAIStats.AttackType.Deathblow)PlaySound(dyingSounds);
    }

    public void PlayTransitionSound(bool isHighAlert)
    {
        if (isHighAlert)    PlaySound(heavyPatrolTransitionSounds);
        else                PlaySound(lightPatrolTransitionSounds);
    }

    private void PlaySound(AudioClip[] clips)
    {
        if (audioSrc.isPlaying) audioSrc.Stop();

        System.Random rnd = new System.Random();
        int temp = rnd.Next(0, clips.Length);
        audioSrc.PlayOneShot(clips[temp]);
    }
}
