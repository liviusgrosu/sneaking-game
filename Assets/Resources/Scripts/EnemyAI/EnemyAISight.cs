﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAISight : MonoBehaviour
{
    // Enemy properties 
    private EnemyAIAwarness aiSenses;
    public float enemyFOV = 110f; // Enemies max field of view
    private RaycastHit ray;
    private int enemySeeingOffset = 0; // Depending the situation, enemy might have a harder or easier time seeing the player

    // Sight collider variables
    private SphereCollider sightCollider;
    private float sphereColliderRadius = 0.0f;
    private float exposureRatioPP = 0.0f;  // Percentage of how close the player is to the enemy

    // Player variables
    private Vector3 directionPP; // Direction player prescence
    private float angleOfPP; // Angle of player prescence

    LayerMask lightMask = ~(1 << 13);

    private void Start() {
        sightCollider = GetComponent<SphereCollider>();
        sphereColliderRadius = sightCollider.radius;

        aiSenses = transform.parent.GetComponent<EnemyAIAwarness>();
    }

    //This still needs to deal with obstructions
    private void OnTriggerStay(Collider other) {
        if(other.tag == "Player") {
            //Calculate how far the player is to the entity and express it as a percentage
            //1 - Far
            //0 - Close
            directionPP = other.transform.position - transform.position;
            exposureRatioPP = directionPP.magnitude / sphereColliderRadius;

            //Check to see if the player is withen the entities line of sight
            angleOfPP = Vector3.Angle(directionPP, transform.forward);
            if (angleOfPP < enemyFOV * 0.5f && exposureRatioPP <= 1.0f) {
                if (Physics.Raycast(transform.position, directionPP, out ray, directionPP.magnitude, lightMask)) {
                    if (ray.collider.tag == "Player" || ray.collider.tag == "Vaulting Hands") {
                        aiSenses.UpdatePlayerVisibilityLevel(other.GetComponent<PlayerDetection>().triggerLevels);
                    } else {
                        aiSenses.UpdatePlayerVisibilityLevel(new float[] { 0f, 0f, 0f });
                    }
                } 
            }
        }
    }

    private void OnTriggerExit(Collider other) {
        if (other.tag == "Player") {
            aiSenses.UpdatePlayerVisibilityLevel(new float[] { 0f, 0f, 0f });
        }
    }
}
