﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAIStats : MonoBehaviour
{
    public EnemySoundController soundController;

    //Temp variables
    public Material deadMat, knockedOutMat;
    private Renderer rend;

    private float health = 100f;

    private enum WellbeingState
    {
        Concious,
        Unconcious,
        Dead
    }

    public enum AttackType
    {
        Regular, 
        Knockout,
        Deathblow
    }


    private WellbeingState currentState;

    // Start is called before the first frame update
    void Start()
    {
        currentState = WellbeingState.Concious;

        rend = GetComponent<Renderer>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void KnockOut()
    {
        currentState = WellbeingState.Unconcious;

        soundController.PlaySound(AttackType.Knockout);

        GetComponent<EnemyAIAwarness>().enabled = false;
        GetComponent<EnemyAIPatrol>().enabled = false;
        GetComponent<EnemyAIFightingController>().enabled = false;

        rend.material = knockedOutMat;
    }

    public void ChangeHealthByIncrememnt(float increment)
    {
        health += increment;
        if (health <= 0)
        {
            soundController.PlaySound(AttackType.Deathblow);

            GetComponent<EnemyAIAwarness>().enabled = false;
            GetComponent<EnemyAIPatrol>().enabled = false;
            GetComponent<EnemyAIFightingController>().enabled = false;

            rend.material = deadMat;
        }
        else soundController.PlaySound(AttackType.Regular);
    }

    public void ChangeHealth(float newHealth)
    {
        health = newHealth;
    }

}
