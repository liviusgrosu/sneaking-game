﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LadderEnds : MonoBehaviour
{
    //If the player collides here then tell the vaulting hand scripts that we ARE at the edge of the ledge
    private void OnTriggerEnter(Collider other) {
        if (other.tag == "Vaulting Hands") {
            other.transform.Find("Vaulting Hands").GetComponent<PlayerLadderClimbing>().UpdateLedgeEndChecks(transform.name, true);
        }
    }
    //If the player exits here then tell the vaulting hand scripts that we ARE NOT at the edge of the ledge
    private void OnTriggerExit(Collider other) {
        if (other.tag == "Vaulting Hands") {
            other.transform.Find("Vaulting Hands").GetComponent<PlayerLadderClimbing>().UpdateLedgeEndChecks(transform.name, false);
        }
    }
}
