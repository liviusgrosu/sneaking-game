﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLeaning : MonoBehaviour
{
    private bool isLeaningLeft, isLeaningRight;

    public Transform leftLeanPoint, rightLeanPoint, restingPoint;
    private Vector3 newLeanPoint;
    private Vector2 leanMod;
    private bool backToRestingPos;
    private bool stanceOverrideControl;

    private Player player;
    private Camera mainCam;
    public PlayerHeadBob headBobSys;

    private RaycastHit leanRay;

    private float timeElapsed = 0f;
    private float maxTimeToLeanPoint = 2f;

    int propLayerMask;

    private void Start() {
        mainCam = Camera.main;
        player = transform.parent.GetComponent<Player>();

        leftLeanPoint = transform.GetChild(0);
        rightLeanPoint = transform.GetChild(1);
        restingPoint = transform.GetChild(2);
        newLeanPoint = Vector3.zero;

        propLayerMask = LayerMask.GetMask("Props");
    }

    // Update is called once per frame
    void Update()
    {
        //
        if (Input.GetKey(KeyCode.Q) && !isLeaningRight && !stanceOverrideControl) {
            CheckAndCalculateForColliders(leftLeanPoint);

            isLeaningLeft = true;
            if (backToRestingPos) backToRestingPos = false;
            if (timeElapsed <= maxTimeToLeanPoint) {
                leanMod = Vector2.Lerp(Vector2.zero, new Vector2(newLeanPoint.x, newLeanPoint.y), timeElapsed / maxTimeToLeanPoint);
                headBobSys.ChangeLeanModPos(leanMod);
                timeElapsed += Time.deltaTime * 12.0f;
            }
        }

        if (Input.GetKeyUp(KeyCode.Q)) {
            backToRestingPos = true;
        }

        if (Input.GetKey(KeyCode.E) && !isLeaningLeft && !stanceOverrideControl) {
            CheckAndCalculateForColliders(rightLeanPoint);

            isLeaningRight = true;
            if (backToRestingPos) backToRestingPos = false;
            if (timeElapsed <= maxTimeToLeanPoint) {
                leanMod = Vector2.Lerp(Vector2.zero, new Vector2(newLeanPoint.x, newLeanPoint.y), timeElapsed / maxTimeToLeanPoint);
                headBobSys.ChangeLeanModPos(leanMod);
                timeElapsed += Time.deltaTime * 12.0f;
            }
        }

        if (Input.GetKeyUp(KeyCode.E)) {
            backToRestingPos = true;
        }

        if (player.IsStanceInTransition() || player.GetProning()) {
            stanceOverrideControl = true;
            backToRestingPos = true;
        }

        if (backToRestingPos) {
            if (timeElapsed >= 0.0f) {
                leanMod = Vector2.Lerp(leanMod, Vector2.zero, (maxTimeToLeanPoint - timeElapsed) / maxTimeToLeanPoint);
                headBobSys.ChangeLeanModPos(leanMod);
                timeElapsed -= Time.deltaTime * 12.0f;
            }
            else {
                timeElapsed = 0.0f;
                headBobSys.ChangeLeanModPos(Vector2.zero);
                leanMod = Vector2.zero;
                backToRestingPos = false;
                stanceOverrideControl = false;

                isLeaningLeft = false;
                isLeaningRight = false;
            }
        }
    }

    void CheckAndCalculateForColliders(Transform intendedLeanPoint) {
        //Make it so that it prematuraly comes near the wall instead of right at it 
        float distToLeanPoint = Vector3.Distance(restingPoint.position + player.GetPreviousCamOffset(), (intendedLeanPoint.position + new Vector3(0, 0.5f, 0f)));
        if (Physics.Raycast(restingPoint.position + player.GetPreviousCamOffset(), (intendedLeanPoint.position + new Vector3(0, 0.5f, 0f)) - restingPoint.position, out leanRay, distToLeanPoint, propLayerMask)) {
            newLeanPoint = leanRay.point;
            newLeanPoint = restingPoint.InverseTransformPoint(newLeanPoint);
        }
        else
            newLeanPoint = intendedLeanPoint.localPosition;
    }

    public bool IsLeaning() {
        return (isLeaningLeft || isLeaningRight);
    }
}
