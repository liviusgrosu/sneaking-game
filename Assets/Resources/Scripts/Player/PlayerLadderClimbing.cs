﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Stop bobbing on the ladder
//Climbing ladder takes too long
//Get off ladder more easily
//Add faster ladder climbing

public class PlayerLadderClimbing : MonoBehaviour
{
    private Player player;
    private CapsuleCollider playerCollider;
    private BoxCollider playerHands;

    private Transform restingPoint;
    private GameObject peakingPoint;
    private GameObject pointBeforePeaking;
    private GameObject ladderObj;
    private GameObject finalPos;
    private GameObject restingToFinalCentrePos;

    [HideInInspector] public bool hangingOntoLadder;
    private bool readyToCatchLadder;
    private bool ableToClimb;
    private bool ableToDismountFromBottom;
    [HideInInspector] public bool isCurrentlyVaulting;
    [HideInInspector] public bool currMovingOnLadder;
    private bool movingFaster;
    private bool doneMountingToTheLadder;

    private float timeElapsed = 0f;
    private float startTime;
    private float maxTransitionTimeToRestingPoint = 1f;
    private float maxTransitionFromRestToPoint1 = 100f;

    [HideInInspector] public bool camIsRestricted;
    private bool isPeaking;
    private float speedMod = 1f;

    private void Start() {
        player = transform.parent.GetComponent<Player>();
        playerCollider = transform.parent.GetComponent<CapsuleCollider>();
        playerHands = GetComponent<BoxCollider>();
    }

    private void OnTriggerEnter(Collider other) {
        if(other.tag == "Ladder") {
            if (!hangingOntoLadder) {
                if (!readyToCatchLadder) readyToCatchLadder = true;
            }

            ladderObj = other.gameObject;
            restingPoint = ladderObj.transform.parent.GetChild(0);
        }

        if(other.tag == "Ladder Ends") {
            UpdateLedgeEndChecks(other.transform.name, true);
        }
    }

    private void OnTriggerExit(Collider other) {
        if (other.tag == "Ladder Ends") {
            UpdateLedgeEndChecks(other.transform.name, false);
        }
    }

    private void Update() {
        if (readyToCatchLadder) {
            hangingOntoLadder = true;
            readyToCatchLadder = false;
            doneMountingToTheLadder = false;

            camIsRestricted = true;
            ableToDismountFromBottom = false;

            restingPoint.transform.position = new Vector3(restingPoint.transform.position.x, player.transform.position.y + 0.3f, restingPoint.transform.position.z);

            CalculatePlayerCameraRestriction();
        }
        if(hangingOntoLadder) {
            if(!doneMountingToTheLadder && timeElapsed <= maxTransitionTimeToRestingPoint && Vector3.Distance(player.transform.position, restingPoint.transform.position) > 0.1f) {
                Vector3 playerXZ = Vector3.Lerp(player.transform.position, restingPoint.transform.position, timeElapsed / maxTransitionTimeToRestingPoint);
                player.transform.position = playerXZ;
                timeElapsed += Time.deltaTime;
            }
            else {
                doneMountingToTheLadder = true;
                ableToClimb = true;
            }

            if (ableToClimb) {
                //If the peak key is released then go back to the resting point
                if (Input.GetKeyUp(KeyCode.LeftControl)) {
                    isPeaking = false;
                    timeElapsed = 0.0f;
                    Destroy(peakingPoint);
                }

                if (Input.GetAxisRaw("Vertical") != 0) {
                    currMovingOnLadder = true;

                    speedMod = 1.0f;

                    if (movingFaster && Input.GetAxisRaw("Vertical") == 1) speedMod = 1.5f;
                    else if (movingFaster && Input.GetAxisRaw("Vertical") == -1) speedMod = 6f;

                    player.transform.Translate(Input.GetAxisRaw("Vertical") * 0.006f * speedMod * ladderObj.transform.up, Space.World);
                }
                else currMovingOnLadder = false;

                //At any time that they are hanging onto the ledge, they may freely drop down from it 
                if (Input.GetKey(KeyCode.Space) && !isPeaking) ResetVariables();
                if (Input.GetKeyDown(KeyCode.LeftShift)) movingFaster = true;
                if (Input.GetKeyUp(KeyCode.LeftShift)) movingFaster = false;
            }
        }

        if(isCurrentlyVaulting) {
            CalculateVaultingProcess();
        }
    }

    private void InitVaultingProcess() {

        //Timer for the vault position slerping
        startTime = Time.time;

        //Disable certain player colliders to avoid unecessary collisions with the platform and the ledge collider
        playerCollider.enabled = false;
        playerHands.enabled = false;

        //Instantiate the final vaulting point and the affecting curve point for the vault movement
        //One point forward + top & another point backward + top
        finalPos = new GameObject();
        restingToFinalCentrePos = new GameObject();

        finalPos.transform.position = player.transform.position + (player.transform.forward * 1.3f) + (player.transform.up * 1.35f);
        restingToFinalCentrePos.transform.position = player.transform.position + (player.transform.forward * -1.8f) + (player.transform.up * 0.4f);
    }

    private void CalculateVaultingProcess() {
        //If the player hasn't reached the final position...
        if (Vector3.Distance(player.transform.position, finalPos.transform.position) > 0.1f) {

            // Interpolate over the arc relative to center
            Vector3 restRelCenter = player.transform.position - restingToFinalCentrePos.transform.position;
            Vector3 point1RelCenter = finalPos.transform.position - restingToFinalCentrePos.transform.position;

            //Change the speed of vaulting movement depending on what part they're at in the vaulting movement
            float fracMod = 1.0f;
            if (Time.time - startTime < 1f) fracMod = 1.5f ;
            else fracMod = 2.85f;

            //Percentage of slerping complete
            float fracComplete = (Time.time - startTime) / maxTransitionFromRestToPoint1;

            //Slerp the player movement 
            player.transform.position = Vector3.Slerp(restRelCenter, point1RelCenter, fracComplete * fracMod);
            player.transform.position += restingToFinalCentrePos.transform.position;
        }
        else {
            //Reset all variables
            playerHands.enabled = true;
            playerCollider.enabled = true;
            ResetVariables();
        }
    }

    //Function that updates the states of whether we are or not near the edge of a ledge
    public void UpdateLedgeEndChecks(string name, bool state) {
        if (ableToClimb) {
            if (name == "Top End Point") {
                InitVaultingProcess();
                isCurrentlyVaulting = true;
                hangingOntoLadder = false;
            }
            else if (name == "Bottom End Point") {
                ResetVariables();
            }
        }
    }

    //Calculates the players camera restrictions when on the ledge
    private void CalculatePlayerCameraRestriction() {
        Vector3 ledgeAngle = -ladderObj.transform.right;

        //Current angle from the player to the ledge
        float angle = Vector3.SignedAngle(ledgeAngle, player.transform.forward, player.transform.up);
        //Get the direction that faces the ledge. This will be the reference point
        float diff = player.transform.eulerAngles.y - angle;

        //X def counter clockwise from the reference point is the left restricition
        float leftRest = diff - 60f;
        //X def clockwise from the reference point is the left restricition
        float rightRest = diff + 60f;

        //Check to see that any of the restrictions go below 0 deg or past 360 deg
        if (leftRest < 0f) leftRest = 360 - (60f - diff);
        if (rightRest > 360f) rightRest -= 360f;

        //Update the player code with these camera restrictions
        player.UpdateCameraAngleRestrictions(leftRest, rightRest);
    }

    //Reset all variables from this script 
    private void ResetVariables() {
        hangingOntoLadder = false;
        ableToClimb = false;

        readyToCatchLadder = false;
        isCurrentlyVaulting = false;

        ladderObj = null;
        restingPoint = null;

        timeElapsed = 0f;

        camIsRestricted = false;

        //This is here because if it wasnt, the player would be stuck once vaulted or dropped from a ledge for a moment because
        //the timer of the jumping code in the player script is still going
        player.SetJumpingState(false);

        StartCoroutine(DisableCollidersMomentarily());
    }

    IEnumerator DisableCollidersMomentarily() {

        playerHands.enabled = false;
        yield return new WaitForSeconds(0.5f);
        playerHands.enabled = true;

    }
}
