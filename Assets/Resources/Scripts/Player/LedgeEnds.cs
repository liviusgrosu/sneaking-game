﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LedgeEnds : MonoBehaviour
{   
    //If the player collides here then tell the vaulting hand scripts that we ARE at the edge of the ledge
    private void OnTriggerEnter(Collider other) {
        if(other.tag == "Player") {
            other.transform.Find("Vaulting Hands").GetComponent<PlayerVaulting>().UpdateLedgeEndChecks(transform.name, true);
        }
    }
    //If the player exits here then tell the vaulting hand scripts that we ARE NOT at the edge of the ledge
    private void OnTriggerExit(Collider other) {
        if (other.tag == "Player") {
            other.transform.Find("Vaulting Hands").GetComponent<PlayerVaulting>().UpdateLedgeEndChecks(transform.name, false);
        }
    }
}
