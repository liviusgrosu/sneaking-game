﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDetection : MonoBehaviour {
    public float playerLightLevelRatio;
    public ArrayList lightSet;

    [Header("Base Trigger Distances")]
    public float baseCautiousLevel = 1.5f;
    public float baseInvestigationLevel = 1.0f;
    public float baseAlertLevel = 0f;
    
    [Space(5)]
    [Header("Increments of Trigger Distances")]
    public float incCautiousLevel = 0.165f;
    public float incInvestigationLevel = 0.333f;
    public float incAlertLevel = 0.666f;

    private float currCautiousLevel;
    private float currInvestigationLevel;
    private float currAlertLevel;

    public float[] triggerLevels;

    private float visibilityMod = 1f;

    private void Start() {
        lightSet = new ArrayList();
        triggerLevels = new float[] { 0f, 0f, 0f };
    }

    private void Update() {
        CalculatePlayerLightLevel();
    }

    //Go through all the lights affecting the player and calculate the overal player visibility 
    public void CalculatePlayerLightLevel() {
        playerLightLevelRatio = 0;

        foreach (LightNode light in lightSet) {
            playerLightLevelRatio += light.currLightPlayerVisibility;
        }

        playerLightLevelRatio = (Mathf.Clamp(playerLightLevelRatio, 0, 100) / 10f) * visibilityMod;

        if (playerLightLevelRatio > 0f) {
            triggerLevels[2] = (playerLightLevelRatio * incAlertLevel) + baseAlertLevel;
            triggerLevels[1] = (playerLightLevelRatio * incInvestigationLevel) + baseInvestigationLevel + triggerLevels[2];
            triggerLevels[0] = (playerLightLevelRatio * incCautiousLevel) + baseCautiousLevel + triggerLevels[1];
        }
        else 
            triggerLevels = new float[] { 0f, 0f, 0f };
    }

    //Add a light to the list of lights affecting the players visibility
    public void AddLightToList(Transform light) {
        lightSet.Add(light.GetComponent<LightNode>());
    }

    //Remove a light to the list of lights affecting the players visibility
    public void RemoveLightFromList(Transform light) {
        lightSet.Remove(light.GetComponent<LightNode>());
    }

    //Change the visibility modifier
    public void ChangeVisibilityModifier(float modifier)
    {
        visibilityMod = modifier;
    }
}
