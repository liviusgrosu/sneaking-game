﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryMap : MonoBehaviour
{
    [HideInInspector]public bool toggleMap;
    public GameObject currMap;

    public Transform mapPageIndicatorParentObj;
    public GameObject mapPageIndicatorPrefab;
    private List<GameObject> mapPageIndicatorBuffer;

    public GameObject mapEntryPrefab;
    public Transform mapParentObj;
    public List<GameObject> testMapPages;
    public bool addTestMaps;
    private List<GameObject> mapEntryBuffer;

    private int totalMapPageCount = 0;
    private int currMapPage = 0;

    private void Start()
    {
        mapPageIndicatorBuffer = new List<GameObject>();
        mapEntryBuffer = new List<GameObject>();

        if (addTestMaps && testMapPages.Count != 0)
        {
            foreach (GameObject obj in testMapPages)
            {
                AddMap(obj.GetComponent<Map>());
            }
            UpdateMapPageCount();
        }

        currMap.SetActive(false);
    }

    public void ToggleMap()
    {
        toggleMap = !toggleMap;

        if(toggleMap)
        {
            ToggleOn();
        }
        else
        {
            ToggleOff();
        }
    }

    public void AddMap(Map map)
    {
        mapEntryBuffer.Add(Instantiate(mapEntryPrefab, mapParentObj.position, mapEntryPrefab.transform.rotation));
        mapEntryBuffer[mapEntryBuffer.Count - 1].GetComponent<MapItemContainer>().SetItem(map, GetComponent<InventoryMap>());
        AddMapToBuffer(mapEntryBuffer, mapParentObj);
    }

    void AddMapToBuffer(List<GameObject> buffer, Transform bufferParentobj)
    {
        //Set the rotation, parent, and the scale of the note buffer index
        buffer[buffer.Count - 1].transform.parent = bufferParentobj;
        buffer[buffer.Count - 1].transform.localEulerAngles = new Vector3(0f, 0f, 0f);
        buffer[buffer.Count - 1].transform.localScale *= 0.45f;

        //Translate it to the correct place in the list
        int mapEntryNumber = buffer.Count - 1;
        buffer[buffer.Count - 1].transform.localPosition = Vector3.zero + new Vector3(0, -0.075f * mapEntryNumber, 0);
        buffer[buffer.Count - 1].SetActive(false);

        UpdateMapPageCount();
    }

    private void UpdateMapPageCount()
    {
        for(int i = mapPageIndicatorBuffer.Count; i < mapEntryBuffer.Count; i++)
        {
            //Add it to the list
            mapPageIndicatorBuffer.Add(Instantiate(mapPageIndicatorPrefab, mapPageIndicatorParentObj.position, Quaternion.identity));
            mapPageIndicatorBuffer[i].transform.parent = mapPageIndicatorParentObj;
            mapPageIndicatorBuffer[i].transform.localEulerAngles = new Vector3(0f, 0f, 0f);
            mapPageIndicatorBuffer[i].transform.localPosition += new Vector3(i * 0.035f, 0f, 0f);
            //Initialize the button
            mapPageIndicatorBuffer[i].GetComponent<MapPageIndicator>().InitButton(i, GetComponent<InventoryMap>());
            //Change it to the unselect sprite
            mapPageIndicatorBuffer[i].GetComponent<MapPageIndicator>().Unselect();
            
            mapPageIndicatorBuffer[i].SetActive(false);
        }
    }

    public void MapEntryButtonClick(int mapEntryNum)
    {
        for (int i = 0; i < mapPageIndicatorBuffer.Count; i++)
        {
            if(i == mapEntryNum)
            {
                mapPageIndicatorBuffer[i].GetComponent<MapPageIndicator>().Select();
                continue;
            }
            mapPageIndicatorBuffer[i].GetComponent<MapPageIndicator>().Unselect();
        }
        
        currMap.GetComponent<Image>().sprite = mapEntryBuffer[mapEntryNum].GetComponent<MapItemContainer>().mapImg;
    }

    private void ToggleOn()
    {
        currMap.SetActive(true);
        if (mapPageIndicatorBuffer.Count > 0)
        {
            for (int i = 0; i < mapPageIndicatorBuffer.Count; i++)
            {
                mapPageIndicatorBuffer[i].SetActive(true);
                if (i == 0) mapPageIndicatorBuffer[i].GetComponent<MapPageIndicator>().Select();
                else mapPageIndicatorBuffer[i].GetComponent<MapPageIndicator>().Unselect();
            }

            MapEntryButtonClick(0);
        }
    }

    private void ToggleOff()
    {
        currMap.SetActive(false);
        for (int i = 0; i < mapPageIndicatorBuffer.Count; i++)
        {
            mapPageIndicatorBuffer[i].SetActive(false);
        }
    }
}
