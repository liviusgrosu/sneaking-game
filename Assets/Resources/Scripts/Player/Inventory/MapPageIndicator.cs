﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MapPageIndicator : MonoBehaviour
{
    private Image image;
    public Sprite select, unselect;
    private int mapEntryNum;
    private InventoryMap invMap;

    private void Awake()
    {
        image = GetComponent<Image>();
    }

    public void InitButton(int mapEntryNum, InventoryMap invMap)
    {
        this.mapEntryNum = mapEntryNum;
        this.invMap = invMap;
    }
    //Select function for the note entry page indicator
    public void Select()
    {
        image.sprite = select;
    }
    //Deselect function for the note entry page indicator
    public void Unselect()
    {
        image.sprite = unselect;
    }
    //Note entry page indicator button click
    public void OnClick()
    {
        invMap.MapEntryButtonClick(mapEntryNum);
    }
}
