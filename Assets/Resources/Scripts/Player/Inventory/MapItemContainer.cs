﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MapItemContainer : MonoBehaviour
{
    public InventoryMap invMap;
    public Map map;
    public Sprite mapImg;

    public void SetItem(Map map, InventoryMap invMap)
    {
        this.map = map;
        this.invMap = invMap;
        mapImg = map.mapImage;
    }
}
