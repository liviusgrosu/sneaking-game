﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class NoteItemContanier : MonoBehaviour
{
    public Note note;

    public Text headerTextUI;

    public int noteIndex;
    private InventoryNotes invNotes;
    public int numOfPages;

    public void SetItem(Note note, InventoryNotes invNotes, int noteIndex) {
        this.note = note;
        headerTextUI.text = note.noteHeader;
        this.invNotes = invNotes;
        this.numOfPages = note.numOfPages;
        this.noteIndex = noteIndex;
    }

    //Note entry button click
    public void OnClick() {
        invNotes.NoteButtonClick(noteIndex);
    }

    //Get the page text for the note entry
    public string GetPageText(int pageNum) {
        string result = "";
        bool startCollecting = false;
        //Temp, fix this afterwards
        //Get all the lines of text from the note file
        string[] lines = System.IO.File.ReadAllLines(@"D:\Users\Liv\Documents\Unity\Sneaking Game Master\Sneaking Game\Assets\Resources\Data Files\Note Data\" + note.noteFileName + ".txt");
        
        //Only collect the given page of the note into the result
        foreach (string line in lines) {
            if (line == "Page " + (pageNum + 1)) {
                startCollecting = true;
                continue;
            }
            if (line == "Page " + (pageNum + 2)) break;
            if(startCollecting) result += line;
            
        }
        return result;
    }
}
