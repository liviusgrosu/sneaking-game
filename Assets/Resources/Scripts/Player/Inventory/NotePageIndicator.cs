﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NotePageIndicator : MonoBehaviour
{
    private Image image;
    private Button but;
    public Sprite select, unselect;
    private int pageNum;
    private InventoryNotes invNotes;

    private void Awake() {
        image = GetComponent<Image>();
        but = GetComponent<Button>();
    }

    public void InitButton(int pageNum, InventoryNotes invNotes) {
        this.pageNum = pageNum;
        this.invNotes = invNotes;
    }
    //Select function for the page indicator
    public void Select() {
        image.sprite = select;
    }
    //Deselect function for the page indicator
    public void Unselect() {
        image.sprite = unselect;
    }
    //Page indicator button click
    public void OnClick() {
        invNotes.PageButtonClick(pageNum);
    }
}
