﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour
{
    public string itemName;
    public int itemID;
    public string itemDescription;
    public GameObject itemModel;
    public GameObject worldObject;

    public bool isConsumable;

    public enum ItemPrimaryType 
    {
        Equipment, 
        Tool, 
        Note, 
        Loot
    }
    public ItemPrimaryType primaryType;

    public enum ItemSecondaryType {
        Weapon, 
        MapPiece, 
        Quest, 
    }

    public ItemSecondaryType secondaryType;
}
