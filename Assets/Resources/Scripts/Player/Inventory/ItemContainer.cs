﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemContainer : MonoBehaviour
{
    public Item item;

    public Item GetItem() {
        return item;
    }
}
