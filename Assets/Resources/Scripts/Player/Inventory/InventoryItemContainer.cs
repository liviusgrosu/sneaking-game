﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryItemContainer : MonoBehaviour
{
    public Item item;

    public int itemCount = 0;
    [HideInInspector] public GameObject itemModel;
    public Text nameUI, countUI;

    public void SetItem(Item item) {
        this.item = item;
        nameUI.text = this.item.itemName;

        if (!item.isConsumable)
            countUI.enabled = false;
        else
            AddCount();
    }

    public void AddCount() {
        itemCount++;
        countUI.text = itemCount.ToString();
    }

    public void RemoveCount() {
        itemCount--;
        countUI.text = itemCount.ToString();
    }
}
