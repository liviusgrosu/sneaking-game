﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BookAnimationEventPointer : MonoBehaviour
{
    private InventoryBook book;

    // Start is called before the first frame update
    void Start()
    {
        book = transform.parent.GetComponent<InventoryBook>();
    }

    void AnimationHasStarted()
    {
        book.AnimationHasStarted();
    }

    void AnimationHasFinished()
    {
        book.AnimationHasFinished();
    }
}
