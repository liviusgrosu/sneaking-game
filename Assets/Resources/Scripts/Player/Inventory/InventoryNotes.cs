﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryNotes : MonoBehaviour {

    //Prefab for a note 
    //Instantiate the prefab with the content of the notes

    private bool toggleNotePages;
    private int currNoteBookButtonPage = 0;
    private int totalNoteBookButtonPages = 0;

    [Space(5)]
    [Header("Note Page Indicator Variables")]
    public GameObject notePageIndicatorPrefab;
    public Transform notePageIndicatorsObj;
    private List<GameObject> notePageIndicatorBuffer;

    [Space(5)]
    [Header("Note Page Entry Variables")]
    private bool readingANote;
    public Text notePageText; //The text of the page entry
    public GameObject noteEntryPageIndicatorPrefab;
    public GameObject notePageBackButtonPrefab; //The back button prefab
    public Transform noteEntryPageIndicatorsParentObj; //The page indicators that activate when a page entry is viewed
    private List<GameObject> noteEntryPageIndicatorBuffer;
    private NoteItemContanier currPageEntryItem;

    [Space(5)]
    [Header("Note Buffer & Test Notes")]
    public GameObject noteBufferIndexPrefab;
    public Transform notebookBufferObj;
    public List<GameObject> notesBuffer;
    public List<GameObject> testNotes;
    public bool addTestNotes;

    private int maxNotesOnScreen = 9;

    private void Start() {

        //Create new lists for each of these buffers
        notesBuffer = new List<GameObject>();
        notePageIndicatorBuffer = new List<GameObject>();
        noteEntryPageIndicatorBuffer = new List<GameObject>();

        //Add in some test items into the inventory
        if (addTestNotes && testNotes.Count != 0) {
            foreach (GameObject obj in testNotes) {
                //Add the item to inventory
                AddNoteToNoteBook(obj.GetComponent<Note>());
            }
        }
    }

    //Add a note to the buffer
    public void AddNoteToNoteBook(Note note) {
        //Create the a note entry prefab and add it to the buffer
        notesBuffer.Add(Instantiate(noteBufferIndexPrefab, notebookBufferObj.position, Quaternion.identity));
        //Set the Note component to this buffer index
        notesBuffer[notesBuffer.Count - 1].GetComponent<NoteItemContanier>().SetItem(note, GetComponent<InventoryNotes>(), notesBuffer.Count - 1);
        AddNoteToBuffer(notesBuffer, notebookBufferObj);
        UpdateNoteBookPageCount();
    }

    //Add a note to the buffer
    void AddNoteToBuffer(List<GameObject> notesBuffer, Transform notebookBufferObj) {
        //Set the rotation, parent, and the scale of the note buffer index
        notesBuffer[notesBuffer.Count - 1].transform.parent = notebookBufferObj;
        notesBuffer[notesBuffer.Count - 1].transform.localEulerAngles = new Vector3(0f, 0f, 0f);
        notesBuffer[notesBuffer.Count - 1].transform.localScale *= 0.45f;

        //Translate it to the correct place in the list
        int noteNumber = (notesBuffer.Count - 1) % maxNotesOnScreen;
        notesBuffer[notesBuffer.Count - 1].transform.localPosition = Vector3.zero + new Vector3(0, -0.075f * noteNumber, 0);
        notesBuffer[notesBuffer.Count - 1].SetActive(false);
    }

    //Toggle the note entries
    public void ToggleNotePages() {
        toggleNotePages = !toggleNotePages;

        if (toggleNotePages) {
            ToggleNotesOn();
        }
        else {
            //If the player was reading a note, then disable that window
            if(readingANote) NoteBackButtonClick();
            ToggleNotesOff();
        }
    }

    //Update the page count for the note page entries 
    void UpdateNoteBookPageCount() {
        //Calculate the amount of note page indicators
        totalNoteBookButtonPages = ((notesBuffer.Count - 1) / maxNotesOnScreen) + 1;

        //Add more page indicators to the list
        if (notePageIndicatorBuffer.Count < totalNoteBookButtonPages)
        {
            for(int i = notePageIndicatorBuffer.Count; i < totalNoteBookButtonPages; i++) {
                //Add it to the list
                notePageIndicatorBuffer.Add(Instantiate(notePageIndicatorPrefab, notePageIndicatorsObj.position, Quaternion.identity));
                notePageIndicatorBuffer[i].transform.parent = notePageIndicatorsObj;
                notePageIndicatorBuffer[i].transform.localEulerAngles = new Vector3(0f, 0f, 0f);
                notePageIndicatorBuffer[i].transform.localPosition += new Vector3(i * 0.035f, 0f, 0f);
                //Initialize the button
                notePageIndicatorBuffer[i].GetComponent<NotePageIndicator>().InitButton(i, GetComponent<InventoryNotes>());
                //Change it to the unselect sprite
                notePageIndicatorBuffer[i].GetComponent<NotePageIndicator>().Unselect();
                notePageIndicatorBuffer[i].SetActive(false);
            }
        }
    }

    //Toggle the note list on
    void ToggleNotesOn() {

        //Go through the notes on a given page and activate them
        for (int i = currNoteBookButtonPage * maxNotesOnScreen; i < currNoteBookButtonPage * maxNotesOnScreen + maxNotesOnScreen; i++) {
            //If its the last page and past the last note entry then stop
            if (i >= notesBuffer.Count) break;
            notesBuffer[i].SetActive(true);
        }

        //Set the page indicators to active and select the current page
        for (int i = 0; i < totalNoteBookButtonPages; i++) {
            notePageIndicatorBuffer[i].SetActive(true);
            if (currNoteBookButtonPage == i) notePageIndicatorBuffer[i].GetComponent<NotePageIndicator>().Select();
        }
    }

    //Toggle the note list off
    void ToggleNotesOff() {
        //Go through the notes on a given page and deactivate them
        for (int i = currNoteBookButtonPage * maxNotesOnScreen; i < currNoteBookButtonPage * maxNotesOnScreen + maxNotesOnScreen; i++) {
            if (i >= notesBuffer.Count) break;
            notesBuffer[i].SetActive(false);
        }

        //Set the page indicators to inactive
        for (int i = 0; i < totalNoteBookButtonPages; i++) {
            notePageIndicatorBuffer[i].SetActive(false);
        }
    }

    //Function that handles when the next page indicator gets clicked on
    public void PageButtonClick(int pageNum) {
        //Turn off all the current page list
        for (int i = currNoteBookButtonPage * maxNotesOnScreen; i < currNoteBookButtonPage * maxNotesOnScreen + maxNotesOnScreen; i++) {
            if (i >= notesBuffer.Count) break;
            notesBuffer[i].SetActive(false);
        }

        //Unselect the current page indicator
        //Select the new page indicator
        notePageIndicatorBuffer[currNoteBookButtonPage].GetComponent<NotePageIndicator>().Unselect();
        currNoteBookButtonPage = pageNum;
        notePageIndicatorBuffer[currNoteBookButtonPage].GetComponent<NotePageIndicator>().Select();

        //Turn on the page list that should exist on the new given page number
        for (int i = currNoteBookButtonPage * maxNotesOnScreen; i < currNoteBookButtonPage * maxNotesOnScreen + maxNotesOnScreen; i++) {
            if (i >= notesBuffer.Count) break;
            notesBuffer[i].SetActive(true);
        }
    }

    //Function that handles a page entry button click given the note entry index
    public void NoteButtonClick(int noteIndex) {
        //The player is reading a note entry
        readingANote = true;
        //Get the current note item container
        currPageEntryItem = notesBuffer[noteIndex].GetComponent<NoteItemContanier>();
        //Set the note entry text to on
        notePageText.gameObject.SetActive(true);
        //Get the first page of the note entry and display it on a text
        notePageText.text = currPageEntryItem.GetPageText(0);

        //Turn off the note list and the page indicators
        ToggleNotesOff();

        //Add in the back button
        noteEntryPageIndicatorBuffer.Add(Instantiate(notePageBackButtonPrefab, noteEntryPageIndicatorsParentObj.position, notePageBackButtonPrefab.transform.rotation));
        noteEntryPageIndicatorBuffer[0].GetComponent<NoteEntryBackButton>().InitButton(GetComponent<InventoryNotes>());
        noteEntryPageIndicatorBuffer[0].transform.parent = noteEntryPageIndicatorsParentObj;

        //Add in a new set of page indicators for that note entry
        for (int i = 1; i < currPageEntryItem.numOfPages + 1; i++) {
            noteEntryPageIndicatorBuffer.Add(Instantiate(noteEntryPageIndicatorPrefab, noteEntryPageIndicatorsParentObj.position, noteEntryPageIndicatorPrefab.transform.rotation));
            noteEntryPageIndicatorBuffer[i].GetComponent<NoteEntryPageIndicator>().InitButton(i - 1, GetComponent<InventoryNotes>());
            noteEntryPageIndicatorBuffer[i].transform.parent = noteEntryPageIndicatorsParentObj;
            //Set the first one to select as the note entry starts there
            if(i == 1) noteEntryPageIndicatorBuffer[i].GetComponent<NoteEntryPageIndicator>().Select();
            else noteEntryPageIndicatorBuffer[i].GetComponent<NoteEntryPageIndicator>().Unselect();
        }

        //Have to do this here or else the orientation and placement of the page number indicators get messed up 
        for(int i = 0; i < currPageEntryItem.numOfPages + 1; i++) {
            noteEntryPageIndicatorBuffer[i].transform.localEulerAngles = new Vector3(0f, 0f, 0f);
            noteEntryPageIndicatorBuffer[i].transform.localPosition += new Vector3(i * 0.07f, 0f, 0f);
        }
    }

    //Note entry back button clicked on
    public void NoteBackButtonClick() {
        //Reset the note page entry variables
        readingANote = false;
        notePageText.gameObject.SetActive(false);
        
        //Destroy all the note entry page indicators
        for (int i = 0; i < currPageEntryItem.numOfPages + 1; i++) {
            Destroy(noteEntryPageIndicatorBuffer[i]);
        }
        //Clear its list
        noteEntryPageIndicatorBuffer.Clear();
        //Turn on the note list and the page indicators 
        ToggleNotesOn();
    }

    //The page indicator for the note entry is pressed on
    public void NoteEntryPageButtonClick(int pageEntryNum) {
        //Change the text to the new given text from the page entry
        notePageText.text = currPageEntryItem.GetPageText(pageEntryNum);

        //Select the new page indicator
        for(int i = 1; i < currPageEntryItem.numOfPages + 1; i++) {
            if ((i - 1) == pageEntryNum) noteEntryPageIndicatorBuffer[i].GetComponent<NoteEntryPageIndicator>().Select();
            else noteEntryPageIndicatorBuffer[i].GetComponent<NoteEntryPageIndicator>().Unselect();
        }
    }
}
