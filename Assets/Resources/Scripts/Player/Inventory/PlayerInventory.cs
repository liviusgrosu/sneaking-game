﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerInventory : MonoBehaviour
{
    //Toggle variable that stores the state of the inventory
    [HideInInspector] public bool toggleInv;

    //Player hands
    //This might be replaced with an animation transition
    public GameObject hands;

    //Menu System
    private PlayerMenu menu;

    //Variables that deal with an item being picked up
    private GameObject itemLookedAt;
    private RaycastHit lookAtRay;
    private LayerMask interactableItemMask;

    //Inventory buffer and test items
    public GameObject inventoryBufferIndexPrefab; //The inventory item prefab 
    public Transform leftInventoryBufferObj, rightInventoryBufferObj; //Parent transform that stores the items as children
    public List<GameObject> leftInventoryBuffer, rightInventoryBuffer; //The inventory buffer represented as data
    public List<GameObject> testItems; //Some test items to play around with

    //Currently held items
    public GameObject leftItemHeld, rightItemHeld;
    public PlayerHandsController leftHandController;

    public Transform leftSpawnDespawnPoint, rightSpawnDespawnPoint;
    private bool itemAnimationGoing, itemLeftHandHelpAnimationGoing, itemRightHandHelpAnimationGoing;
    private bool initItemAnimation;

    //Animation variables that deals with the inventory opening the inv, closing the inv, choosing an item, replacing and item, and deselecting an item
    private float timeElapsed = 0f;
    private float timeElapsed2 = 0f;
    private float maxTransitionTimeFromCentreToCircleEdge = 0.05f;
    private Vector3 targetLeftCircleEdge, targetRightCircleEdge;
    private int itemAnimationIte;
    private float leftCircleFragmentAngle, rightCircleFragmentAngle;
    private float currLeftCircleAngle, currRightCircleAngle;

    //Item description text elements
    public Text leftItemDescriptionText, rightItemDescriptionText;

    private void Start() {
        menu = transform.parent.GetComponent<PlayerMenu>();

        interactableItemMask = 1 << 19;

        leftInventoryBuffer = new List<GameObject>();
        rightInventoryBuffer = new List<GameObject>();

        //Add in some test items into the inventory
        if (testItems.Count != 0) {
            foreach (GameObject obj in testItems) {
                //Add the item to inventory
                AddItemToInventory(obj.GetComponent<Item>());
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        //If the inventory item toggle is on, calculate its animation
        if(itemAnimationGoing) {
            CalculateInventoryItemsAnimation();
        }

        //If the single item select toggle is on, calculate its animation
        if(itemLeftHandHelpAnimationGoing || itemRightHandHelpAnimationGoing) {
            CalculateSingleInventoryItemAnimation();
        }
    }

    //Toggle the inventory menu on/off
    public void ToggleInv() {
        if (!itemAnimationGoing) {
            toggleInv = !toggleInv;

            if (toggleInv) {
                ToggleOn();
            }
            else {
                ToggleOff();
            }
        }
    }

    //Toggle the inventory ON
    void ToggleOn() {

        //If the book menu is up, then turn it off
        if (menu.GetBookState()) menu.ToggleBook();
        //If the inventory book is off, then turn it on
        if (!menu.GetBookObjState()) menu.ToggleBookObj();

        hands.SetActive(true); //Turn the hands on
        itemAnimationGoing = true; //The animation has started, no input is allowed as this happens
        //menu.LockAndHideMouse(false); //Enable the use of a mouse

        //Turn on both hands
        leftItemDescriptionText.enabled = true;
        rightItemDescriptionText.enabled = true;
    }

    //Toggle the inventory OFF
    void ToggleOff() {

        //If the book menu is off and the inventory book is still on, then turn off the inventory book
        //this is needed if the player clicks on the book in menu since its closing this menu
        if (!menu.GetBookState() && menu.GetBookObjState()) menu.ToggleBookObj();

        hands.SetActive(false); //Turn the hands off
        itemAnimationGoing = true; //The animation has started, no input is allowed as this happens

        //Turn off both hands
        leftItemDescriptionText.enabled = false;
        rightItemDescriptionText.enabled = false;
    }

    //Add an item that has the item component
    public void AddItemToInventory(Item item) {

        //Check to see which hand this item belongs to
        //Assign the target hand buffer and buffer transform
        List<GameObject> targetBuffer;
        Transform targetBufferObj;
        if (item.primaryType == Item.ItemPrimaryType.Equipment) {
            targetBuffer = leftInventoryBuffer;
            targetBufferObj = leftInventoryBufferObj;
        }
        else if (item.primaryType == Item.ItemPrimaryType.Tool) {
            targetBuffer = rightInventoryBuffer;
            targetBufferObj = rightInventoryBufferObj;
        }
        else {
            targetBuffer = new List<GameObject>();
            targetBufferObj = leftInventoryBufferObj; //For now just set it to the left hand 
        }

        //Check to see if the item exists in the buffer
        //If it does then add it to the buffer
        foreach (GameObject itemIndex in targetBuffer) {
            if (item.itemID == itemIndex.GetComponent<InventoryItemContainer>().item.GetComponent<Item>().itemID) {
                itemIndex.GetComponent<InventoryItemContainer>().AddCount();
                return;
            }
        }

        //Add it to the inventory
        targetBuffer.Add(Instantiate(inventoryBufferIndexPrefab, targetBufferObj.position, Quaternion.identity));

        //Get the item data, set its parent to the inventory buffer object, and set it to inactive
        targetBuffer[targetBuffer.Count - 1].GetComponent<InventoryItemContainer>().SetItem(item.GetComponent<Item>());

        //Create the item model and assign it to the inventory item
        targetBuffer[targetBuffer.Count - 1].GetComponent<InventoryItemContainer>().itemModel = Instantiate(item.GetComponent<Item>().itemModel, targetBuffer[targetBuffer.Count - 1].transform.position, Quaternion.identity);

        //Add the item to the buffer
        AddItemToBuffer(targetBuffer, targetBufferObj);
    }

    //Add an item that has the item container component
    public void AddItemToInventory(ItemContainer item) {

        //Check to see which hand this item belongs to
        //Assign the target hand buffer and buffer transform
        List<GameObject> targetBuffer;
        Transform targetBufferObj;
        if (item.item.primaryType == Item.ItemPrimaryType.Equipment) {
            targetBuffer = leftInventoryBuffer;
            targetBufferObj = leftInventoryBufferObj;
        }
        else if (item.item.primaryType == Item.ItemPrimaryType.Tool) {
            targetBuffer = rightInventoryBuffer;
            targetBufferObj = rightInventoryBufferObj;
        }
        else {
            targetBuffer = new List<GameObject>();
            targetBufferObj = leftInventoryBufferObj; //For now just set it to the left hand 
        }

        //Check to see if the item exists in the buffer
        //If it does then add it to the buffer
        foreach (GameObject itemIndex in targetBuffer) {
            if (item.GetItem().itemID == itemIndex.GetComponent<InventoryItemContainer>().item.GetComponent<Item>().itemID) {
                itemIndex.GetComponent<InventoryItemContainer>().AddCount();
                return;
            }
        }

        //Add it to the inventory
        targetBuffer.Add(Instantiate(inventoryBufferIndexPrefab, leftInventoryBufferObj.transform.position, Quaternion.identity));

        //Get the item data, set its parent to the inventory buffer object, and set it to inactive
        targetBuffer[targetBuffer.Count - 1].GetComponent<InventoryItemContainer>().SetItem(item.GetComponent<ItemContainer>().GetItem());

        //Create the item model and assign it to the inventory item
        targetBuffer[targetBuffer.Count - 1].GetComponent<InventoryItemContainer>().itemModel = Instantiate(item.GetComponent<ItemContainer>().GetItem().itemModel, targetBuffer[targetBuffer.Count - 1].transform.position, Quaternion.identity);

        //Add the item to the buffer
        AddItemToBuffer(targetBuffer, targetBufferObj);
        
    }

    void AddItemToBuffer(List<GameObject> targetBuffer, Transform targetBufferObj) {
        //Set the models parent as the inventory item prefab transform
        targetBuffer[targetBuffer.Count - 1].GetComponent<InventoryItemContainer>().itemModel.transform.parent = targetBuffer[targetBuffer.Count - 1].transform;
        //Set the models scale to small
        targetBuffer[targetBuffer.Count - 1].GetComponent<InventoryItemContainer>().itemModel.transform.localScale *= 0.2f;
        targetBuffer[targetBuffer.Count - 1].GetComponent<InventoryItemContainer>().itemModel.transform.localEulerAngles = new Vector3(-45f, 90f, 0f);

        //Set the inventory item prefabs parents as the buffer object transform
        targetBuffer[targetBuffer.Count - 1].transform.parent = targetBufferObj;
        //Set the scale of the inventory item prefab to something larger
        targetBuffer[targetBuffer.Count - 1].transform.localScale *= 1.3f;
        //Reset the rotation. This might need to be changed to something else
        targetBuffer[targetBuffer.Count - 1].transform.localEulerAngles = new Vector3(0f, 0f, 0f);

        //Set the active state of the inventory item prefab to false
        targetBuffer[targetBuffer.Count - 1].SetActive(false);
    }

    //Calculate for an item being held
    public void AddItemToAHand(GameObject item) {
        //If the item is from the left hand inventory...
        if (item.GetComponent<InventoryItemContainer>().item.primaryType == Item.ItemPrimaryType.Equipment) {
            //If there isnt an animation going...
            if (!itemLeftHandHelpAnimationGoing && !itemRightHandHelpAnimationGoing) {
                //If the item selected is being held then add it back to the inventory buffer
                //The held item is empty
                //Reset the item description
                if (leftItemHeld == item) {
                    leftInventoryBuffer.Add(leftItemHeld);
                    leftItemHeld = null;
                    //Remove the equipment from the equipment hand controller
                    leftHandController.RemoveEquipmentWorldObj();
                    leftItemDescriptionText.text = "";
                }
                else {
                    //If another item is being selected then get rid of the previous item and reset the held item
                    if (leftItemHeld != null) {
                        leftInventoryBuffer.Add(leftItemHeld);
                        leftItemHeld = null;
                        //Remove the equipment from the equipment hand controller
                        leftHandController.RemoveEquipmentWorldObj();
                    }
                    //Add the chosen item to the held item container
                    leftItemHeld = item;
                    //Give the equipment hand controller the equipment
                    leftHandController.GiveEquipmentWorldObj(leftItemHeld.GetComponent<InventoryItemContainer>().item.GetComponent<Item>().worldObject);
                    leftInventoryBuffer.Remove(item);

                    //Update the item description text
                    leftItemDescriptionText.text = leftItemHeld.GetComponent<InventoryItemContainer>().item.GetComponent<Item>().itemDescription;
                }
                //Start the animation process
                itemLeftHandHelpAnimationGoing = true;
            }
        }
        //If the item is from the right hand inventory...
        else if (item.GetComponent<InventoryItemContainer>().item.primaryType == Item.ItemPrimaryType.Tool) {
            //If there isnt an animation going...
            if (!itemLeftHandHelpAnimationGoing && !itemRightHandHelpAnimationGoing) {
                //If the item selected is being held then add it back to the inventory buffer
                //The held item is empty
                //Reset the item description
                if (rightItemHeld == item) {
                    rightInventoryBuffer.Add(rightItemHeld);
                    rightItemHeld = null;

                    rightItemDescriptionText.text = "";
                }
                else {
                    //If another item is being selected then get rid of the previous item and reset the held item
                    if (rightItemHeld != null) {
                        rightInventoryBuffer.Add(rightItemHeld);
                        rightItemHeld = null;
                    }
                    //Add the chosen item to the held item container
                    rightItemHeld = item;
                    rightInventoryBuffer.Remove(item);

                    //Update the item description text
                    rightItemDescriptionText.text = rightItemHeld.GetComponent<InventoryItemContainer>().item.GetComponent<Item>().itemDescription;
                }
                //Start the animation process
                itemRightHandHelpAnimationGoing = true;
            }
        }
    }

    //This just deals with the process of selecting/deselecting items onto a hand 
    void CalculateSingleInventoryItemAnimation() {

        if(!initItemAnimation) {
            //Reset the needed timers
            itemAnimationIte = 0;
            timeElapsed = 0f;
            timeElapsed2 = 0f;
            //Divide the circle into the amount of items in the inventory
            //Find its place on a corresponding hands circles edge
            leftCircleFragmentAngle = 360f / (float)leftInventoryBuffer.Count;
            rightCircleFragmentAngle = 360f / (float)rightInventoryBuffer.Count;
            currLeftCircleAngle = leftCircleFragmentAngle;
            currRightCircleAngle = rightCircleFragmentAngle;
            //Start at the first position of the circle edge; 0
            targetLeftCircleEdge = targetRightCircleEdge = new Vector3(0.3f * Mathf.Sin(0f), 0.3f * Mathf.Cos(0f), 0f);

            //Initalization of the animation is complete
            initItemAnimation = true;
        }

        //Move the chosen left item into the middle of the circle
        if (itemLeftHandHelpAnimationGoing && leftItemHeld != null && timeElapsed2 <= maxTransitionTimeFromCentreToCircleEdge) {
            leftItemHeld.transform.localPosition = Vector3.Lerp(leftItemHeld.transform.localPosition, leftSpawnDespawnPoint.localPosition, timeElapsed / maxTransitionTimeFromCentreToCircleEdge);
            timeElapsed2 += Time.deltaTime;
        }
        //Once thats done, put it precisely on where the item being held should be
        //This avoid any margin of error
        else if(itemLeftHandHelpAnimationGoing && leftItemHeld != null && timeElapsed2 >= maxTransitionTimeFromCentreToCircleEdge) {
            leftItemHeld.transform.localPosition = leftSpawnDespawnPoint.localPosition;
        }

        //Move the chosen right item into the middle of the circle
        if (itemRightHandHelpAnimationGoing && rightItemHeld != null && timeElapsed2 <= maxTransitionTimeFromCentreToCircleEdge) {
            rightItemHeld.transform.localPosition = Vector3.Lerp(rightItemHeld.transform.localPosition, rightSpawnDespawnPoint.localPosition, timeElapsed / maxTransitionTimeFromCentreToCircleEdge);
            timeElapsed2 += Time.deltaTime;
        }
        //Once thats done, put it precisely on where the item being held should be
        //This avoid any margin of error
        else if (itemRightHandHelpAnimationGoing && rightItemHeld != null && timeElapsed2 >= maxTransitionTimeFromCentreToCircleEdge) {
            rightItemHeld.transform.localPosition = rightSpawnDespawnPoint.localPosition;
        }

        //If theres still some items positions that need to be calculated for...
        if (itemAnimationIte < leftInventoryBuffer.Count || itemAnimationIte < rightInventoryBuffer.Count) {

            if (timeElapsed <= maxTransitionTimeFromCentreToCircleEdge) {
                //If theres still some items on the left inventory buffer
                if (itemLeftHandHelpAnimationGoing && itemAnimationIte < leftInventoryBuffer.Count) {
                    //Move between the spawn and the target area of the circle
                    leftInventoryBuffer[itemAnimationIte].transform.localPosition = Vector3.Lerp(leftInventoryBuffer[itemAnimationIte].transform.localPosition, leftSpawnDespawnPoint.localPosition + targetLeftCircleEdge, timeElapsed / maxTransitionTimeFromCentreToCircleEdge);
                }
                //If theres still some items on the right inventory buffer
                if (itemRightHandHelpAnimationGoing && itemAnimationIte < rightInventoryBuffer.Count) {
                    //Move between its current location and the despawn zone
                    rightInventoryBuffer[itemAnimationIte].transform.localPosition = Vector3.Lerp(rightInventoryBuffer[itemAnimationIte].transform.localPosition, rightSpawnDespawnPoint.transform.localPosition + targetRightCircleEdge, timeElapsed / maxTransitionTimeFromCentreToCircleEdge);
                }
                timeElapsed += Time.deltaTime;
            }
            else {
                //Reset variables for the next item
                if (itemLeftHandHelpAnimationGoing && itemAnimationIte < leftInventoryBuffer.Count) {
                    //Once thats done, put it precisely on where the item should be
                    //This avoid any margin of error
                    leftInventoryBuffer[itemAnimationIte].transform.localPosition = leftSpawnDespawnPoint.localPosition + targetLeftCircleEdge;
                    targetLeftCircleEdge = new Vector3(0.3f * Mathf.Sin(Mathf.Deg2Rad * currLeftCircleAngle), 0.3f * Mathf.Cos(Mathf.Deg2Rad * currLeftCircleAngle), 0f);
                }
                if (itemRightHandHelpAnimationGoing && itemAnimationIte < rightInventoryBuffer.Count) {
                    //Once thats done, put it precisely on where the item should be
                    //This avoid any margin of error
                    rightInventoryBuffer[itemAnimationIte].transform.localPosition = rightSpawnDespawnPoint.transform.localPosition + targetRightCircleEdge;
                    targetRightCircleEdge = new Vector3(0.3f * Mathf.Sin(Mathf.Deg2Rad * currRightCircleAngle), 0.3f * Mathf.Cos(Mathf.Deg2Rad * currRightCircleAngle), 0f);
                }

                timeElapsed = 0f;
                //Go to the next angle of the circle
                if (itemLeftHandHelpAnimationGoing) currLeftCircleAngle += leftCircleFragmentAngle;
                if (itemRightHandHelpAnimationGoing) currRightCircleAngle += rightCircleFragmentAngle;
                itemAnimationIte++;
            }
        }
        else {
            //Stop this process
            if(itemLeftHandHelpAnimationGoing) itemLeftHandHelpAnimationGoing = false;
            if(itemRightHandHelpAnimationGoing) itemRightHandHelpAnimationGoing = false;
            initItemAnimation = false;
        }
    }

    //Function that deals with the animation of the inventory items
    void CalculateInventoryItemsAnimation() {

        //Toggle ON animation
        if (toggleInv) {
            //Start the animation
            if (!initItemAnimation) {
                leftInventoryBufferObj.gameObject.SetActive(true);
                rightInventoryBufferObj.gameObject.SetActive(true);

                itemAnimationIte = 0;
                timeElapsed = 0f;
                //Divide the circle into the amount of items in the inventory
                //Find its place on a circles edge
                leftCircleFragmentAngle = 360f / (float)leftInventoryBuffer.Count;
                rightCircleFragmentAngle = 360f / (float)rightInventoryBuffer.Count;
                currLeftCircleAngle = leftCircleFragmentAngle;
                currRightCircleAngle = rightCircleFragmentAngle;
                targetLeftCircleEdge = targetRightCircleEdge = new Vector3(0.3f * Mathf.Sin(0f), 0.3f * Mathf.Cos(0f), 0f);

                initItemAnimation = true;
            }

            //Do this process to all the items in the inventory
            if (itemAnimationIte < leftInventoryBuffer.Count || itemAnimationIte < rightInventoryBuffer.Count) {
                if (timeElapsed <= maxTransitionTimeFromCentreToCircleEdge) {
                    //Go through the left hand inventory items
                    if (itemAnimationIte < leftInventoryBuffer.Count) {
                        //Set the item is active
                        if (!leftInventoryBuffer[itemAnimationIte].activeSelf) leftInventoryBuffer[itemAnimationIte].SetActive(true);
                        //Move between the spawn and the target area of the circle
                        leftInventoryBuffer[itemAnimationIte].transform.localPosition = Vector3.Lerp(leftInventoryBuffer[itemAnimationIte].transform.localPosition, leftSpawnDespawnPoint.localPosition + targetLeftCircleEdge, timeElapsed / maxTransitionTimeFromCentreToCircleEdge);
                    }
                    //Go through the right hand inventory items
                    if (itemAnimationIte < rightInventoryBuffer.Count) {
                        //Set the item is active
                        if (!rightInventoryBuffer[itemAnimationIte].activeSelf) rightInventoryBuffer[itemAnimationIte].SetActive(true);
                        //Move between the spawn and the target area of the circle
                        rightInventoryBuffer[itemAnimationIte].transform.localPosition = Vector3.Lerp(rightInventoryBuffer[itemAnimationIte].transform.localPosition, rightSpawnDespawnPoint.localPosition + targetRightCircleEdge, timeElapsed / maxTransitionTimeFromCentreToCircleEdge);
                    }
                    timeElapsed += Time.deltaTime;
                }
                else {
                    //Reset variables for the next item
                    if (itemAnimationIte < leftInventoryBuffer.Count) {
                        leftInventoryBuffer[itemAnimationIte].transform.localPosition = leftSpawnDespawnPoint.localPosition + targetLeftCircleEdge;
                        targetLeftCircleEdge = new Vector3(0.3f * Mathf.Sin(Mathf.Deg2Rad * currLeftCircleAngle), 0.3f * Mathf.Cos(Mathf.Deg2Rad * currLeftCircleAngle), 0f);
                    }
                    if (itemAnimationIte < rightInventoryBuffer.Count) {
                        rightInventoryBuffer[itemAnimationIte].transform.localPosition = rightSpawnDespawnPoint.localPosition + targetRightCircleEdge;
                        targetRightCircleEdge = new Vector3(0.3f * Mathf.Sin(Mathf.Deg2Rad * currRightCircleAngle), 0.3f * Mathf.Cos(Mathf.Deg2Rad * currRightCircleAngle), 0f);
                    }
                    timeElapsed = 0f;
                    currLeftCircleAngle += leftCircleFragmentAngle;
                    currRightCircleAngle += rightCircleFragmentAngle;
                    itemAnimationIte++;
                }
            }
            else {
                //Stop this process
                itemAnimationGoing = false;
                initItemAnimation = false;
            }
        }
        //Toggle OFF animation
        else {
            //Start the animation
            if (!initItemAnimation) {
                itemAnimationIte = 0;
                timeElapsed = 0f;
                initItemAnimation = true;
            }

            //Do this process to all the items in the inventory
            if (itemAnimationIte < leftInventoryBuffer.Count || itemAnimationIte < rightInventoryBuffer.Count) {
                if (timeElapsed <= maxTransitionTimeFromCentreToCircleEdge) {
                    //Go through the left hand inventory items
                    if (itemAnimationIte < leftInventoryBuffer.Count) {
                        //Move between its current location and the despawn zone
                        leftInventoryBuffer[itemAnimationIte].transform.localPosition = Vector3.Lerp(leftInventoryBuffer[itemAnimationIte].transform.localPosition, leftInventoryBufferObj.transform.localPosition, timeElapsed / maxTransitionTimeFromCentreToCircleEdge);
                    }
                    //Go through the right hand inventory items
                    if (itemAnimationIte < rightInventoryBuffer.Count) {
                        //Move between its current location and the despawn zone
                        rightInventoryBuffer[itemAnimationIte].transform.localPosition = Vector3.Lerp(rightInventoryBuffer[itemAnimationIte].transform.localPosition, rightInventoryBufferObj.transform.localPosition, timeElapsed / maxTransitionTimeFromCentreToCircleEdge);
                    }
                    timeElapsed += Time.deltaTime;
                }
                else {
                    //Reset variables for the next item
                    timeElapsed = 0f;
                    //Set the inventory items to inactive for each respective hands
                    if (itemAnimationIte < leftInventoryBuffer.Count) leftInventoryBuffer[itemAnimationIte].SetActive(false);
                    if (itemAnimationIte < rightInventoryBuffer.Count) rightInventoryBuffer[itemAnimationIte].SetActive(false);
                    itemAnimationIte++;
                }
            }
            else {
                //Stop this process
                leftInventoryBufferObj.gameObject.SetActive(false);
                rightInventoryBufferObj.gameObject.SetActive(false);
                itemAnimationGoing = false;
                initItemAnimation = false;
            }
        }
    }

    public GameObject GetWorldItemToLeftHand()
    {
        if (leftItemHeld) return leftItemHeld.GetComponent<InventoryItemContainer>().item.GetComponent<Item>().worldObject;
        else return null;
    }

    public bool DecrementEquipmentCount()
    {
        if(leftItemHeld.GetComponent<InventoryItemContainer>().item.GetComponent<Item>().isConsumable)
        {
            leftItemHeld.GetComponent<InventoryItemContainer>().RemoveCount();
            if (leftItemHeld.GetComponent<InventoryItemContainer>().itemCount <= 0)
            {
                Destroy(leftItemHeld);
                leftItemDescriptionText.text = "";
                return true;
            }
        }
        return false;
    }
}
