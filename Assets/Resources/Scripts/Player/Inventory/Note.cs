﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Note : MonoBehaviour
{
    public string noteHeader;
    public int noteID;
    public string noteFileName;
    public int numOfPages;
}
