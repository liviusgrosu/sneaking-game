﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoteEntryBackButton : MonoBehaviour
{
    private InventoryNotes invNotes;

    public void InitButton(InventoryNotes invNotes) {
        this.invNotes = invNotes;
    }

    //Note entry back button click
    public void OnClick() {
        invNotes.NoteBackButtonClick();
    }
}
