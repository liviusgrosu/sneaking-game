﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NoteEntryPageIndicator : MonoBehaviour
{
    private Image image;
    public Sprite select, unselect;
    private int pageEntryNum;
    private InventoryNotes invNotes;

    private void Awake() {
        image = GetComponent<Image>();
    }

    public void InitButton(int pageEntryNum, InventoryNotes invNotes) {
        this.pageEntryNum = pageEntryNum;
        this.invNotes = invNotes;
    }
    //Select function for the note entry page indicator
    public void Select() {
        image.sprite = select;
    }
    //Deselect function for the note entry page indicator
    public void Unselect() {
        image.sprite = unselect;
    }
    //Note entry page indicator button click
    public void OnClick() {
        invNotes.NoteEntryPageButtonClick(pageEntryNum);
    }
}
