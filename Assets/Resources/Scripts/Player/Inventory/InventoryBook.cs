﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryBook : MonoBehaviour
{
    //Current map: Stays on when book is closed 
    //Current note: Returns back to note menu when book is closed

    //Hover the book a bit more up when mouse is selected over it
    //Hovering the book reveals the title of it

    private PlayerMenu menu;
    public Animator bookAnim;
    private InventoryNotes notes;
    private InventoryMap map;
    private bool WaitUntilBookIsClosedToDisable;

    private enum CurrentBookPos {
        Resting, 
        Hovering, 
        Opened
    }
    private CurrentBookPos currPos;

    [HideInInspector] public bool toggleBookObj, toggleBook;
    
    private bool bookOpeningAnimationGoing, bookHoveringAnimationGoing;
    private bool initHoveringAnimation;
    private bool doneOpeningClosingAnimation;

    [Header("Objects")]
    public GameObject bookObj;
    private Transform targetPoint;
    [Header("Move Points")]
    public Transform restingPoint;
    public Transform hoverPoint;
    public Transform openPoint;

    private float timeElapsed;
    private float maxTimeToHover = 0.2f;

    Ray bookRay;
    RaycastHit bookHit;

    // Start is called before the first frame update
    void Start()
    {
        doneOpeningClosingAnimation = true;

        menu = transform.parent.GetComponent<PlayerMenu>();
        notes = transform.GetComponent<InventoryNotes>();
        map = transform.GetComponent<InventoryMap>();
        currPos = CurrentBookPos.Resting;
    }

    // Update is called once per frame
    void Update()
    {
        //This is only when the inventory menu is on since you can access the book from there
        if (toggleBookObj && !toggleBook && !bookOpeningAnimationGoing) {
            bookRay = Camera.main.ScreenPointToRay(Input.mousePosition);
            //Only look for objects with the UI layer
            if (Physics.Raycast(bookRay, out bookHit, Mathf.Infinity, 1 << 5) && bookHit.collider.tag == "Book Collider") {
                //If the player is hovering over the book in the inventory menu
                //Start to hover the book
                if (!bookHoveringAnimationGoing && currPos == CurrentBookPos.Resting) {
                    bookHoveringAnimationGoing = true;
                    targetPoint = hoverPoint;
                    currPos = CurrentBookPos.Hovering;
                }

                //This triggers the book menu to open
                if (Input.GetKeyDown(KeyCode.Mouse0))  ToggleBook();
            }
            //If nothing points to the book in the inventory menu, then return it to the resting point
            else {
                if (!bookHoveringAnimationGoing && currPos == CurrentBookPos.Hovering) {
                    bookHoveringAnimationGoing = true;
                    targetPoint = restingPoint;
                    currPos = CurrentBookPos.Resting;
                }
            }
        }

        //Calculate for book movement animation
        if(bookHoveringAnimationGoing) {
            CalculateHoverAnimation();
        }

        //Turn the book obj off when its closed
        if (WaitUntilBookIsClosedToDisable) {
            if (bookAnim.GetCurrentAnimatorStateInfo(0).IsName("Idle")) {
                ToggleBookObj();
                WaitUntilBookIsClosedToDisable = false;
            }
        }

        
    }

    //Deals with the book movement animation
    void CalculateHoverAnimation() {

        //Initalize the animation
        if(!initHoveringAnimation) {
            timeElapsed = 0f;
            initHoveringAnimation = true;
        }

        //Move the book to the target
        if (timeElapsed <= maxTimeToHover) {
            bookObj.transform.position = Vector3.Lerp(bookObj.transform.position, targetPoint.position, timeElapsed / maxTimeToHover);
            timeElapsed += Time.deltaTime;
        }
        //Stop this process
        else {
            timeElapsed = 0f;
            bookHoveringAnimationGoing = false;
            targetPoint = null;
            initHoveringAnimation = false;
        }
        
    }

    //Toggle the closed book (found in the inventory menu) on/off
    public void ToggleBookObj() {
        if (!bookOpeningAnimationGoing) {
            toggleBookObj = !toggleBookObj;

            if (toggleBookObj) ToggleObjOn();
            else ToggleObjOff();
        }
    }

    //Toggle the closed book object on
    void ToggleObjOn() {
        bookObj.SetActive(true);
        //Closed book starts in the resting position
        bookObj.transform.position = restingPoint.position;
    }

    //Toggle the closed book object off
    void ToggleObjOff() {
        bookObj.SetActive(false);
    }

    //Toggle the book menu
    public void ToggleBook() {
        if (doneOpeningClosingAnimation)
        {
            toggleBook = !toggleBook;

            if (toggleBook) ToggleOn();
            else ToggleOff();
        }
    }

    //Toggle the book menu on
    void ToggleOn() {
            //This happens when the player is opening this menu up from no current menu opened
            if (!toggleBookObj) ToggleBookObj();
            //Turn off the inventory menu if its on
            if (menu.GetInventoryState()) menu.ToggleInventory();

            //Start the book oepning animation
            bookHoveringAnimationGoing = true;
            targetPoint = openPoint;
            currPos = CurrentBookPos.Opened;
            bookAnim.SetTrigger("OpenBook");
            notes.ToggleNotePages(); // Change this to when the book opens
            map.ToggleMap();
    }

    //Toggle the book menu off
    void ToggleOff() {
        //Put the book back in its resting place like it is when the inventory menu is on
        if (menu.GetInventoryState())
        {
            bookHoveringAnimationGoing = true;
            targetPoint = restingPoint;
            currPos = CurrentBookPos.Resting;
            bookAnim.SetTrigger("CloseBook");
            notes.ToggleNotePages(); // Change this to when the book closes
            map.ToggleMap();
        }
        //Turn off the book 
        else
        {
            //Turn the book obj off when its closed
            WaitUntilBookIsClosedToDisable = true;
            bookAnim.SetTrigger("CloseBook");
            currPos = CurrentBookPos.Resting;
            targetPoint = restingPoint;
            notes.ToggleNotePages(); // Change this to when the book closes
            map.ToggleMap();
        }
    }

    public void AnimationHasStarted()
    {
        if (bookAnim.GetCurrentAnimatorStateInfo(0).IsName("Book Close"))
        {
            doneOpeningClosingAnimation = true;
        }
        else
        {
            doneOpeningClosingAnimation = false;
        }
    }

    public void AnimationHasFinished()
    {
        if (bookAnim.GetCurrentAnimatorStateInfo(0).IsName("Book Close"))
        {
            doneOpeningClosingAnimation = false;
        }
        else
        {
            doneOpeningClosingAnimation = true;
        }
    }
}
