﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFootController : MonoBehaviour
{
    public GameObject leftFoot;
    public GameObject rightFoot;

    protected Player player;

    public float proneSync;
    public float crouchSync;
    public float walkingSync;
    public float runningSync;

    private bool leftFootActive, rightFootActive, isCycling;

    private bool isGrounded;

    RaycastHit ray;

    private void Start() {
        //Start with the left foot active
        leftFootActive = true;
        leftFoot.SetActive(leftFootActive);
        rightFoot.SetActive(leftFootActive);

        Physics.IgnoreLayerCollision(2, 8);

        player = transform.parent.GetComponent<Player>();
    }

    //Enumerator that cycles between each active foot 
    public void CycleFeet(int val) {
        //Left foot is now active and it checks for the floor collider and plays a sound
        if (leftFootActive) {
            leftFootActive = false;
            rightFootActive = true;
            if (val == 1) rightFoot.GetComponent<PlayerFoot>().PlayAgainWalk();
        }

        //Right foot is now active and it checks for the floor collider and plays a sound
        else if (rightFootActive) {
            leftFootActive = true;
            rightFootActive = false;
            if (val == 1) leftFoot.GetComponent<PlayerFoot>().PlayAgainWalk();
        }

        //Wait 1 second in between each foot activation
        rightFoot.SetActive(rightFootActive);
        leftFoot.SetActive(leftFootActive);
    }

    private void OnTriggerStay(Collider other) {
        //If the players foot is touching the floor then they are grounded
        isGrounded = true;
    }
    private void OnTriggerExit(Collider other) {
        //Once they leave the floor, the player is no longer grounded
        isGrounded = false;
    }

    public bool IsTouchingGround() {
        return isGrounded;
    }
}
