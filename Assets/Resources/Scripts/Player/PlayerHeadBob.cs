﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHeadBob : MonoBehaviour {
    public Player player;
    public PlayerFootController feet;
    public PlayerLeaning leaningSys;
    public GameObject playerHands;
    public Vector3 restPosition; //local position where your camera would rest when it's not bobbing.
    private Vector3 modCamPos;
    public float transitionSpeed = 20f; //smooths out the transition from moving to not moving.
    public float bobSpeed = 4.8f; //how quickly the player's head bobs.
    public float bobAmount = 0.05f; //how dramatic the bob is. Increasing this in conjunction with bobSpeed gives a nice effect for sprinting.

    float timer = Mathf.PI / 2; //initialized as this value because this is where sin = 1. So, this will make the camera always start at the crest of the sin wave, simulating someone picking up their foot and starting to walk--you experience a bob upwards when you start walking as your foot pushes off the ground, the left and right bobs come as you walk.
    Vector3 camPos;
    Vector3 handPos;

    private bool justLanded;

    private bool triggerFoot;

    private Vector2 leanMod;

    void Awake() {
        restPosition = transform.localPosition;
        camPos = transform.localPosition;
        handPos = playerHands.transform.localPosition;
    }

    void Update() {
        if (feet.IsTouchingGround() || player.GetLedgeMovement() || player.GetLadderMovement()) {
            WalkingBobHead();
        }
        if (player.GetJumping()) justLanded = false;
        if (player.GetFalling() && feet.IsTouchingGround() && !justLanded) {
            justLanded = true;
            JumpingBobHead();
        }
    }

    void WalkingBobHead() {
        if (feet.IsTouchingGround()) {
            if (player.GetCrouching()) {
                bobSpeed = 3f;
                bobAmount = 0.05f;
            }
            else if (player.GetProning()) {
                bobSpeed = 4f;
                bobAmount = 0.05f;
            }
            else if (player.GetRunning()) {
                bobSpeed = 9f;
                bobAmount = 0.1f;
            }
            else {
                bobSpeed = 4.8f;
                bobAmount = 0.05f;
            }
        }
        else if (player.GetLedgeMovement()) {
            bobSpeed = 4f;
            bobAmount = 0.01f;
        }
        else if (player.GetLadderMovement()) {
            bobSpeed = 2f;
            bobAmount = 0.01f;
        }

        if (player.GetHorizontalAxisRaw() != 0 || player.GetVerticalAxisRaw() != 0) //moving
        {
            timer += bobSpeed * Time.deltaTime;
            //use the timer value to set the position
            Vector3 newPosition = new Vector3(Mathf.Cos(timer) * bobAmount + leanMod.x, restPosition.y + modCamPos.y + leanMod.y + Mathf.Abs((Mathf.Sin(timer) * bobAmount)), restPosition.z); //abs val of y for a parabolic path
            camPos = newPosition;

        }
        else {
            timer = Mathf.PI / 2; //reinitialize

            Vector3 newPosition = new Vector3(Mathf.Lerp(camPos.x, restPosition.x + leanMod.x, transitionSpeed * Time.deltaTime), Mathf.Lerp(camPos.y, restPosition.y + modCamPos.y + leanMod.y, transitionSpeed * Time.deltaTime), Mathf.Lerp(camPos.z, restPosition.z, transitionSpeed * Time.deltaTime)); //transition smoothly from walking to stopping.
            camPos = newPosition;
        }

        //print(timer);
        if (triggerFoot && timer > Mathf.PI * 2) //completed a full cycle on the unit circle. Reset to 0 to avoid bloated values.
        {
            timer = 0;
            TriggerFoot(1);
            triggerFoot = false;
        }
        //Needs a more precice solution
        //Doesnt trigger always
        if (!triggerFoot && Mathf.Abs(timer - Mathf.PI) <= 0.1f) {
            TriggerFoot(1);
            triggerFoot = true;
        }

        transform.localPosition = camPos;
    }

    void JumpingBobHead() {
        TriggerFoot(2);
    }

    void TriggerFoot(int val) {
        feet.CycleFeet(val);
    }

    public void ChangeCamModPos(Vector3 modCamPos) {
        this.modCamPos = modCamPos;
    }

    public void ChangeLeanModPos(Vector2 leanMod) {
        this.leanMod = leanMod;
    }
}