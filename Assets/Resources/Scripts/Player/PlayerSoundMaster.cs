﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSoundMaster : MonoBehaviour {
    private Player player;

    [Header("Walking Sounds")]
    public AudioClip[] walkingSurfaceSounds;

    public float carpetMult = 0f;
    public float woodMult = 2f;
    public float metalMult = 4f;

    private float soundMod = 1f;

    [Space(5)]
    [Header("Player Speed Sound Multipliers")]
    private float currSpeedSoundMult = 0f;
    public float crouchingSoundMult = 0f;
    public float walkingSoundMult = 2f;
    public float runningSoundMult = 4f;

    private float currCautiousTrigger = 0f;
    private float currInvestigationTrigger = 0f;
    private float currAlertTrigger = 0f;

    public GameObject soundSpherePrefab;
    private GameObject soundSphereInsta;

    public AudioSource source;
    private int currSound;

    public float minVolume = 0.3f;
    public float maxVolume = 0.7f;
    private float maxVolumeModifier;

    private bool readyForNextFootstepSound;

    private void Start() {
        maxVolumeModifier = source.volume / 1.0f;
        minVolume *= maxVolumeModifier;
        maxVolume *= maxVolumeModifier;

        player = transform.parent.GetComponent<Player>();

        //CalculateMaxSoundLevel();
    }

    void Update() {
        //Calculate the sound level of the player and create a sound sphere 
       
        if (source.isPlaying) {
            if (soundSphereInsta == null && !readyForNextFootstepSound) {
                CalculateSoundLevel();
                readyForNextFootstepSound = true;

                soundSphereInsta = (GameObject)Instantiate(soundSpherePrefab, transform.position, transform.rotation);

                //Enemy to player sound detection triggers
                soundSphereInsta.GetComponent<PlayerSoundSphere>().triggerLevels = new float[] { currCautiousTrigger, currInvestigationTrigger, currAlertTrigger };

                //print("C: " + currCautiousTrigger + ", I: " + currInvestigationTrigger + ", A: " + currAlertTrigger);

                Destroy(soundSphereInsta, source.clip.length);
            }
        } else {
            if (readyForNextFootstepSound) readyForNextFootstepSound = false;
        }
    }

    //Depending on what the player is standing on, it would change the radius of the sound sphere 
    public void CalculateSoundLevel() {
        float currMaterialMult = 0f;
        float baseC, baseI, baseA;
        float playerSpeed = player.GetSpeedLevel();

        baseC = baseI = baseA = 0f;

        if (playerSpeed == 2f) {
            baseC =  0f;
            baseI = -1f;
            baseA = -2f;
        }
        if (playerSpeed == 5f) {
            baseC = 4f;
            baseI = 2f;
            baseA = 0f;
        }
        if (playerSpeed == 10f) {
            baseC = 6f;
            baseI = 4f;
            baseA = 2f;
        }

        if (currSound == 1) currMaterialMult = carpetMult;
        if (currSound == 2) currMaterialMult = woodMult;  
        if (currSound == 3) currMaterialMult = metalMult; 

        currCautiousTrigger = (baseC + currMaterialMult) * soundMod; 
        currInvestigationTrigger = (baseI + currMaterialMult) * soundMod;
        currAlertTrigger = (baseA + currMaterialMult) * soundMod;
    }

    //Trigger the sound of the surface the player is stepping on
    public void TriggerSound(int soundIndex) {
        if (soundIndex != 0) {
            currSound = soundIndex;
            source.volume = Mathf.Clamp(player.GetCurrSpeed() / player.GetMaxSpeed() * maxVolumeModifier * soundMod, minVolume, maxVolume);

            source.PlayOneShot(walkingSurfaceSounds[currSound - 1]);
            source.clip = walkingSurfaceSounds[currSound - 1];
        }
    }

    //Check if any foot sound is playing
    public bool IsPlayingSound() {
        return source.isPlaying;
    }

    public void ChangeSoundModifier(float modifier)
    {
        soundMod = modifier;
    }
}
