﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHandsController : MonoBehaviour
{
    private bool holdingUseButton, releasedButton;
    private float maxHoldDownTime = 0.5f;
    private float curHoldDownTime = 0f;

    private PlayerInventory inventory;
    public Transform hand;

    private GameObject equipmentObj;

    private bool isUsing;

    void Start()
    {
        inventory = GameObject.Find("Inventory System").GetComponent<PlayerInventory>();
    }
    void Update()
    {
        if (equipmentObj != null)
        {
            if (equipmentObj.GetComponent<IEquipment>().GetToggle())
            {

                if(equipmentObj.GetComponent<IEquipment>().GetCurrentAnimationState() == CurrentAnimationState.Idle && equipmentObj.GetComponent<IEquipment>().HasSuccesfullyFired())
                {
                    equipmentObj.GetComponent<IEquipment>().ResetExternalTriggers();
                    if(inventory.DecrementEquipmentCount())
                    {
                        equipmentObj.GetComponent<IEquipment>().ToggleEquipment();
                    }
                }

                if (Input.GetMouseButton(0))
                {
                    if (!isUsing) isUsing = true;
                    curHoldDownTime += Time.deltaTime;
                    if (curHoldDownTime >= maxHoldDownTime)
                    {
                        holdingUseButton = true;
                        equipmentObj.GetComponent<IEquipment>().HoldingButton();
                    }
                }

                if (Input.GetMouseButtonUp(0))
                {
                    if (holdingUseButton) equipmentObj.GetComponent<IEquipment>().ReleaseHold();
                    else equipmentObj.GetComponent<IEquipment>().UseRightAway();

                    curHoldDownTime = 0f;
                    holdingUseButton = false;
                }
            }

            if (Input.GetKeyDown(KeyCode.G))
            {
                equipmentObj.GetComponent<IEquipment>().ToggleEquipment();
            }
        }
    }

    public void GiveEquipmentWorldObj (GameObject obj) 
    {
        if (equipmentObj != null) Destroy(equipmentObj);
        //This needs to change
        //Need to add rotation of the left hand as well
        equipmentObj = Instantiate(obj, obj.transform.position, obj.transform.rotation);
        equipmentObj.transform.parent = hand.transform;
        equipmentObj.transform.localPosition = Vector3.zero;
        equipmentObj.transform.localEulerAngles = obj.transform.eulerAngles;

        //print(equipmentObj.transform.rotation);
    }

    public void RemoveEquipmentWorldObj()
    {
        Destroy(equipmentObj);
    }
}