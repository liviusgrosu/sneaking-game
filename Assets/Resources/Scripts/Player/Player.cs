﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Player : MonoBehaviour {
    [Space(5)]
    [Header("Jumping Variables")]
    //How fast the jump is
    public float capJumpSpeed = 5.0f;
    private float currJumpSpeed = 0.0f;
    public float jumpAcc = 0.2f;

    //How long the jump is gonna be
    public float capJumpTravel = 0.7f;
    private float currJumpTravel = 0.0f;

    private bool isJumping;
    private bool isStartJumping;
    private bool jumpButtonPressed;

    public PlayerFootController playerFeet;
    private bool isGrounded;

    [Space(5)]
    [Header("Gravity Variables")]
    private bool isFalling;
    public float gravityAcc = 0.981f;
    public float capTermalSpeed = 20f;
    private float currTermalSpeed = 2.0f;

    [Space(5)]
    [Header("Walking/Running Variables")]
    public float walkSpeed;
    public float crouchSpeed;
    public float proneSpeed;
    public float runSpeed;
    public float currSpeed;
    private float stanceSpeedModifier = 1.0f;

    private Vector3 xzMove = Vector3.zero;
    private Transform cameraEulerY;
    private float leftRest, rightRest;
    private Vector2 horizontalMovement;
    private Rigidbody rb;
    private CapsuleCollider capCol;

    //---Stance Variables---
    //Heights
    private float startingHeight;
    private float crouchingHeight;
    private float proningHeight;
    //Collider centre changes
    private Vector3 startingColCentre;
    private Vector3 crouchingColCentre;
    private Vector3 proningColCentre;
    //Collider radius changes
    private float startingColRadius;
    private float proningColRadius;
    //Camera stance offset
    private Vector3 startingCamOffset;
    private Vector3 crouchingCamOffset;
    private Vector3 proningCamOffset;
    private Vector3 previousCamOffset;

    private bool stanceInTransition;
    private float camDistToCentre;

    private bool canStandUp;
    private bool isCrouching;
    private bool isProning;
    private bool isRunning;
    private bool forceWalk;
    //----------------------

    [Space(5)]
    [Header("Mouse Variables")]
    public float mouseSensitivity = 5.0f;
    public float upDownRange = 60.0f;
    private float verticalRotation = 0;
    private float horizontalRotation = 0;

    CursorLockMode wantedMouseMode;
    public bool lockMouse;

    [Space(5)]
    [Header("Camera Variables")]

    public GameObject mainCam;
    private PlayerHeadBob headBob;
    public PlayerVaulting vaultingSys;
    public PlayerLadderClimbing ladderClimbingSys;
    public PlayerLeaning leaningSys;
    public PlayerMenu menuSys;

    public Transform standingPos, crouchingPos, proningPos;

    // Start is called before the first frame update
    void Start() {
        rb = GetComponent<Rigidbody>();
        cameraEulerY = new GameObject().transform;
        capCol = GetComponent<CapsuleCollider>();

        headBob = mainCam.GetComponent<PlayerHeadBob>();

        startingHeight = 2f;
        crouchingHeight = 1.3333f;
        proningHeight = 0.8f;

        startingColCentre = new Vector3(0f, 0f, 0f);
        crouchingColCentre = new Vector3(0f, -0.35f, 0f);
        proningColCentre = new Vector3(0f, -0.6f, 0f);

        startingColRadius = 0.5f;
        proningColRadius = 0.4f;

        startingCamOffset = new Vector3(0f, 0f, 0f);
        crouchingCamOffset = new Vector3(0f, -0.6f, 0f);
        proningCamOffset = new Vector3(0f, -1.2f, 0f);
        previousCamOffset = startingCamOffset;

        ToggleMouse(false);
    }

    //Fixed update is called for physics calculations
    void FixedUpdate() {
        if (vaultingSys.hangingOntoLedge || vaultingSys.isCurrentlyVaulting || ladderClimbingSys.hangingOntoLadder || ladderClimbingSys.isCurrentlyVaulting) CalculateLedgeMovement();
        else CalculateMovement();
    }

    //Update is called for player input
    private void Update() {
        //Cant move or look around if the inventory is showing
        if (!menuSys.GetInventoryState() && !menuSys.GetBookState()) {
            CalculateKeyboardInput();
            CalculateCameraRotation();
        }
        //Remove any previous movement
        else {
            if (xzMove.magnitude != 0f) xzMove /= 1.1f;
        }
        canStandUp = true;
    }

    void CalculateMovement() {
        //Check if the player is grounded first
        CheckIfGround();
        //Change movement direction in relation to camera orientation
        Vector3 moveDirection = new Vector3(xzMove.x * currSpeed, 0f, xzMove.y * currSpeed);
        Vector3 moveAndCameraDirection = cameraEulerY.TransformDirection(moveDirection);

        //Only apply gravity if the player is not grounded and not jumping
        if (!isGrounded && !isJumping) {
            //start applying fall velocity once the player has stopped the jumping calculation
            if (!isFalling) {
                isFalling = true;
                currTermalSpeed = 2.0f;
            }

            moveAndCameraDirection = new Vector3(moveAndCameraDirection.x, -currTermalSpeed, moveAndCameraDirection.z);
        }
        else
            moveAndCameraDirection = new Vector3(moveAndCameraDirection.x, 0.0f, moveAndCameraDirection.z);

        //If the player is jumping...
        if (isJumping) {
            //A timer ticks down and once reaches 0 then player starts falling down
            //This controls how high they jump
            //Additionally the speed of the jump decreases the longer it is in the jump
            if (currJumpTravel < capJumpTravel) {
                currJumpTravel += 0.1f;
                currJumpSpeed -= jumpAcc;
            }
            else if (isGrounded) isJumping = false; //Stop jumping if they reach the ground
            else isJumping = false; //Stop jumping once the timer is 0

            moveAndCameraDirection = new Vector3(moveAndCameraDirection.x, currJumpSpeed, moveAndCameraDirection.z);
        }

        //If the player is falling...
        if (isFalling && !stanceInTransition && !vaultingSys.hangingOntoLedge || !ladderClimbingSys.hangingOntoLadder) {
            //Likewise with the jumping timer, the more the player is in their falling sequence the faster they fall
            //The falling speed is capped at a terminal velocity
            if (currTermalSpeed < capTermalSpeed) currTermalSpeed += gravityAcc;
            if (isGrounded) isFalling = false;
        }

        //Apply final velocity to rigidbody
        rb.velocity = moveAndCameraDirection;
    }

    void CalculateLedgeMovement() {
        currTermalSpeed = 0f;
        currJumpTravel = 0f;
        currJumpSpeed = 0f;
        rb.velocity = new Vector3(0f, 0f, 0f);
    }

    //Get keyboard input from player
    void CalculateKeyboardInput() {
        //Get the movement input from the player
        if (GetHorizontalAxisRaw() != 0.0f || GetVerticalAxisRaw() != 0.0f) {
            xzMove = new Vector2(GetHorizontalAxisRaw(), GetVerticalAxisRaw());
            xzMove = xzMove.normalized;
        }
        else {
            //Decelerate the player once they stop moving until they come to a halt
            if (xzMove.magnitude != 0f) xzMove /= 1.1f;
        }

        CheckJumpInput();
        CheckRunningWalkingInput();
        CheckCrouchingInput();
    }

    public float GetHorizontalAxisRaw() {
        return (!menuSys.GetInventoryState() && !menuSys.GetBookState()) ? Input.GetAxisRaw("Horizontal") : 0f;
    }

    public float GetVerticalAxisRaw() {
        return (!menuSys.GetInventoryState() && !menuSys.GetBookState()) ? Input.GetAxisRaw("Vertical") : 0f;
    }

    private void CheckJumpInput() {
        //Start the jump as long as the player is grounded
        if (Input.GetKeyDown(KeyCode.Space) && isGrounded) {
            if (!isCrouching && !isProning) {
                isJumping = true;
                isStartJumping = true;
                jumpButtonPressed = true;
                currJumpTravel = 0.0f;
                currJumpSpeed = capJumpSpeed;
            }
            else if (isCrouching) {
                isCrouching = !isCrouching;
                ChangeStance();
            }
            else if(isProning) {
                isProning = !isProning;
                ChangeStance();
            }
        }
    }

    private void CheckRunningWalkingInput() {
        //Start accelerating the player if they run and cap at the set run speed
        if (Input.GetKey(KeyCode.LeftShift) && !forceWalk && canStandUp && !leaningSys.IsLeaning()) {
            if (isCrouching && !stanceInTransition) {
                isCrouching = !isCrouching;
                ChangeStance();
            }

            if(isProning && !stanceInTransition) {
                isProning = !isProning;
                ChangeStance();
            }

            if (currSpeed < runSpeed) currSpeed += 9f * Time.deltaTime;
            else currSpeed = runSpeed;
            isRunning = true;
        }
        //Deccelerate the player if they stopped running until the set walking speed
        else if ((!Input.GetKey(KeyCode.LeftShift) && isGrounded && !isCrouching && !isProning) || forceWalk || leaningSys.IsLeaning()) {
            if (currSpeed > walkSpeed) {
                currSpeed -= 9f * Time.deltaTime;
            }
            if (currSpeed < walkSpeed) {
                currSpeed += 9f * Time.deltaTime;
            }
            if (Mathf.Abs(currSpeed - walkSpeed) <= 0.1f) {
                currSpeed = walkSpeed;
            }
            isRunning = false;
        }
    }

    private void CheckCrouchingInput() {
        if (Input.GetKeyDown(KeyCode.C)) {
            if (!forceWalk && !stanceInTransition && canStandUp) {
                if (isProning) isProning = false;
                isCrouching = !isCrouching;
                ChangeStance();
            }
        }

        if (Input.GetKeyDown(KeyCode.Z)) {
            if (!forceWalk && !stanceInTransition && canStandUp) {
                if (isCrouching) isCrouching = false;
                isProning = !isProning;
                ChangeStance();
            }
        }

        if (isCrouching) {
            if (currSpeed > crouchSpeed) currSpeed -= 9f * Time.deltaTime;
            else currSpeed = crouchSpeed;
        }

        if (isProning) {
            
            if (currSpeed > proneSpeed) currSpeed -= 9f * Time.deltaTime;
            else currSpeed = proneSpeed;
        }
    }

    public void ForceChangeStance(string stance) {
        if(stance == "Proning") {
            isProning = !isProning;
            ChangeStance();
        }
        else if(stance == "Standing") {
            if (isProning) isProning = false;
            if (isCrouching) isCrouching = false;
            ChangeStance();
        }
    }

    //Get the camera input and apply rotation to the player
    void CalculateCameraRotation() {
        //Get mouse input from the player and clamp it so the player doesnt over turn 
        horizontalRotation = Input.GetAxis("Mouse X") * mouseSensitivity;
        verticalRotation -= Input.GetAxis("Mouse Y") * mouseSensitivity;
        verticalRotation = Mathf.Clamp(verticalRotation, -upDownRange, upDownRange);

        if (vaultingSys.camIsRestricted || ladderClimbingSys.camIsRestricted) {

            //The player cant look too far from the left or the right
            if (leftRest > rightRest) {
                float diff = 360 - transform.eulerAngles.y;

                //This is done so that if the restriction angles overlap past 0 or 360, the system will
                //accomindate for that 
                if(transform.eulerAngles.y < leftRest && transform.eulerAngles.y > rightRest) {
                    if (diff > 180f && horizontalRotation > 0) horizontalRotation = 0;
                    else if(diff < 180f && horizontalRotation < 0) horizontalRotation = 0;
                }
            }
            else {
                if (transform.eulerAngles.y > rightRest && horizontalRotation > 0) horizontalRotation = 0;
                else if (transform.eulerAngles.y < leftRest && horizontalRotation < 0) horizontalRotation = 0;
            }

            //Rotate the character object and the camera from the input of the mouse
            transform.Rotate(0, horizontalRotation, 0);

            Camera.main.transform.localRotation = Quaternion.Euler(verticalRotation, 0, 0);

            //This code stops the player from looking up/down which causes the speed to slow down
            cameraEulerY.eulerAngles = new Vector3(0, Camera.main.transform.eulerAngles.y, 0);
        }
        else {
            //Rotate the character object and the camera from the input of the mouse
            transform.Rotate(0, horizontalRotation, 0);

            Camera.main.transform.localRotation = Quaternion.Euler(verticalRotation, 0, 0);

            //This code stops the player from looking up/down which causes the speed to slow down
            cameraEulerY.eulerAngles = new Vector3(0, Camera.main.transform.eulerAngles.y, 0);

            //print(Camera.main.transform.eulerAngles.y);
        }
    }

    //Check if the player is grounded 
    void CheckIfGround() {
        isGrounded = playerFeet.IsTouchingGround();
    }

    public void ToggleMouse(bool state) {
        if (state) wantedMouseMode = CursorLockMode.Locked;
        else wantedMouseMode = CursorLockMode.None;

        Cursor.lockState = wantedMouseMode;
        Cursor.visible = !state;
    }

    void ChangeStance() {
        if (isCrouching) 
            StartCoroutine(StanceTransition(crouchingHeight, startingColRadius, crouchingColCentre, crouchingCamOffset, 2f));
        else if (isProning) 
            StartCoroutine(StanceTransition(proningHeight, proningColRadius, proningColCentre, proningCamOffset, 2f));
        else 
            StartCoroutine(StanceTransition(startingHeight, startingColRadius, startingColCentre, startingCamOffset, 2f));
    }

    IEnumerator StanceTransition(float targetHeight, float targetRadius,Vector3 targetCentre, Vector3 targetCamChange, float time) { 
        stanceInTransition = true;
        float elapsedTime = 0;

        while (Mathf.Abs(capCol.height - targetHeight) >= 0.01f || Vector3.Distance(capCol.center, targetCentre) >= 0.1f) {
            rb.velocity = new Vector3(rb.velocity.x, 0f, rb.velocity.z);

            capCol.height = Mathf.Lerp(capCol.height, targetHeight, elapsedTime / time);
            capCol.center = Vector3.Lerp(capCol.center, targetCentre, elapsedTime / time);
            capCol.radius = Mathf.Lerp(capCol.radius, targetRadius, elapsedTime / time);

            headBob.ChangeCamModPos(Vector3.Lerp(previousCamOffset, targetCamChange, elapsedTime * 5f / time));

            elapsedTime += Time.deltaTime;
            yield return null;
        }
        capCol.height = targetHeight;
        capCol.center = targetCentre;
        previousCamOffset = targetCamChange;

        GetComponent<CapsuleCollider>().height = targetHeight;
        stanceInTransition = false;
    }

    public bool IsStanceInTransition() {
        return stanceInTransition;
    }

    public bool IsPlayerMoving() {
        return rb.velocity.magnitude > 0.001f;
    }

    //Check if the player is crouching
    public bool GetCrouching() {
        return isCrouching;
    }

    public bool GetProning() {
        return isProning;
    }

    //Check if the player is running
    public bool GetRunning() {
        return isRunning;
    }

    public bool GetFalling() {
        return isFalling;
    }

    public void SetJumpingState(bool state) {
        isJumping = state;
    }

    public bool GetJumping() {
        return isJumping;
    }

    public bool GetStartJumping() {
        return isStartJumping;
    }

    public bool GetVaulting() {
        return vaultingSys.hangingOntoLedge;
    }

    public bool GetLedgeMovement() {
        return vaultingSys.currMovingOnLedge;
    }

    public bool GetLadderMovement() {
        return ladderClimbingSys.currMovingOnLadder;
    }

    public void SetStartJumping(bool state) {
        isStartJumping = state;
    }
    
    //Get the speed level of the player
    public float GetSpeedLevel() {
        float speedPerc = Mathf.Clamp01(rb.velocity.magnitude / (runSpeed));
        return Mathf.Clamp(Mathf.Round(speedPerc * 10f), 0f, 10f);
    }

    public float GetCurrSpeed() {
        return currSpeed;
    }

    //Get the max speed of the player
    public float GetMaxSpeed() {
        return runSpeed;
    }

    public void SetAdditionalForce(Vector3 forceDir, float forceConst) {
        rb.AddForce(forceDir * forceConst);
    }

    //Update the players viewing constraints when in a specific state (ex: hanging off a ledge)
    public void UpdateCameraAngleRestrictions(float leftRest, float rightRest) {
        this.leftRest = leftRest;
        this.rightRest = rightRest;
    }

    public Vector3 GetPreviousCamOffset() {
        return previousCamOffset;
    }
}
