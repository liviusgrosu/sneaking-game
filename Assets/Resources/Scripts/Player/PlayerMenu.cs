﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMenu : MonoBehaviour
{
    private enum CurrMenu {
        Inventory, 
        Books,
        LootBag
    };
    private CurrMenu currMenu;

    public InventoryBook book;
    public InventoryNotes notes;
    public InventoryMap maps;
    public PlayerInventory inv;

    private RaycastHit lookAtRay;

    //Seperate button for the book opening
    //Close the inventory when selecting the book from it

    private void Start() {
        //LockAndHideMouse(true);
    }

    private void Update() {
        //Toggle the inventory when no animations are playing and the button is pressed
        if (Input.GetKeyDown(KeyCode.Tab)) {
            ToggleInventory();
        }

        if (Input.GetKeyDown(KeyCode.B)) {
            book.ToggleBook();
        }


        if (!GetInventoryState() || !GetBookState())
        {
            if (Physics.Raycast(transform.position, transform.forward, out lookAtRay, Mathf.Infinity, 1 << 19) && Input.GetKeyDown(KeyCode.F))
            {
                //As long as the item trying to be picked is an item then add it to the inventory
                if (lookAtRay.collider.tag == "Item")
                {
                    //Add the item to inventory
                    inv.AddItemToInventory(lookAtRay.collider.gameObject.GetComponent<ItemContainer>());

                    //Destroy the object afterwards
                    Destroy(lookAtRay.collider.gameObject);
                }

                if (lookAtRay.collider.tag == "Note Item")
                {
                    //Add the note to the notebook
                    notes.AddNoteToNoteBook(lookAtRay.collider.gameObject.GetComponent<NoteWorldContainer>().note);

                    //Destroy the object afterwards
                    Destroy(lookAtRay.collider.gameObject);
                }

                if(lookAtRay.collider.tag == "Map Item")
                {
                    //Add the note to the notebook
                    maps.AddMap(lookAtRay.collider.gameObject.GetComponent<MapWorldContainer>().map);

                    //Destroy the object afterwards
                    Destroy(lookAtRay.collider.gameObject);
                }
            }
            //If a mouse click occurs on one of the inventory items, then rearange the inventory
            //This calculates for both the left & right hand
            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                Ray mouseRay = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit mouseHit;

                //Only look for objects with the UI layer
                if (Physics.Raycast(mouseRay, out mouseHit, Mathf.Infinity, 1 << 5))
                {
                    if (mouseHit.collider.tag == "Inventory Item")
                    {
                        inv.AddItemToAHand(mouseHit.collider.gameObject);
                    }
                }
            }
        }
    }

    public void ToggleInventory() {
        inv.ToggleInv();
    }

    public void ToggleBook() {
        book.ToggleBook();
    }

    public void ToggleBookObj() {
        book.ToggleBookObj();
    }

    public bool GetInventoryState() {
        return inv.toggleInv;
    }

    public bool GetBookState() {
        return book.toggleBook;
    }

    public bool GetBookObjState() {
        return book.toggleBookObj;
    }

    //Hide/Unhide & lock/unlock the mouse 
    public void LockAndHideMouse(bool state) {
        print(state);
        Cursor.lockState = (state) ? CursorLockMode.Locked : CursorLockMode.None;
        Cursor.visible = !state;
    }
}
