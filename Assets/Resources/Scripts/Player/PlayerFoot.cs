﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFoot : MonoBehaviour {
    private Player player;
    public PlayerSoundMaster soundMixer;
    bool walkPlayed;

    //Reset the sound mixer trigger
    public void PlayAgainWalk() {
        walkPlayed = false;
        player = transform.parent.parent.GetComponent<Player>();
        Physics.IgnoreLayerCollision(0, 8);
    }

    //Check what collider the player is standing on and depending on the tag, play the appropriate sound that it would make
    void OnTriggerStay(Collider other) {
        if (!walkPlayed) {
            int floorMatIndex = 0;
            
            switch(other.tag) {
                case "Carpet":  floorMatIndex = 1; break;
                case "Wood":    floorMatIndex = 2; break;
                case "Stone":   floorMatIndex = 3; break;
                //default:        print(other.name);  break;
            }

            soundMixer.TriggerSound(floorMatIndex);
            walkPlayed = true;
        }
    }
}
