﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PlayerStats : MonoBehaviour
{
    public List<GameObject> currentSelfSpells;

    //Health
    //Fatigue
    //Jump Height 
    //Movement Speed

    private float health = 100f;

    public PlayerDetection playerVisibilityController;
    public PlayerSoundMaster playerSoundController;


    public void RecieveNewSpell(string spellObjName)
    {
        //Load the spell from resources and add it to the current spells list
        GameObject spellInsta = Instantiate(Resources.Load<GameObject>("Prefabs/Spells/" + spellObjName));
        spellInsta.transform.parent = transform;
        spellInsta.GetComponent<ISpells>().OnCreate(currentSelfSpells.Capacity);
        currentSelfSpells.Add(spellInsta);
    }

    public void RemoveSpellFromList(int id)
    {
        GameObject spellToRemove = currentSelfSpells.Single(c => c.GetComponent<ISpells>().GetId() == id);
        currentSelfSpells.Remove(spellToRemove);
        Destroy(spellToRemove);
    }

    public bool HasASpell()
    {
        return false;
    }

    public void ChangeVisibilityMod(float modifier)
    {
        playerVisibilityController.ChangeVisibilityModifier(modifier);
    }

    public void ResetVisibilityMod()
    {
        playerVisibilityController.ChangeVisibilityModifier(1f);
    }

    public void ChangeSoundMod(float modifier)
    {
        playerSoundController.ChangeSoundModifier(modifier);
    }

    public void ResetSoundMod()
    {
        playerSoundController.ChangeSoundModifier(1f);
    }

    public void ChangeHealthByIncrememnt(float increment)
    {
        health += increment;
    }

    public void ChangeHealth(float newHealth)
    {
        health = newHealth;
    }

}
