﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterBoltSpellController : MonoBehaviour, IEquipment
{
    bool isHolding;
    bool isToggled;
    bool isInUse;
    public Animator anim;
    public Renderer rend;

    public Transform projectileSpawn;
    public GameObject projectilePrefab;

    public float minProjectileVelocity = 7.5f;
    public float maxProjectileVelocity = 20f;
    private float currProjectileVelocity = 0f;


    bool setToDeactivate;
    bool doneFiring;

    public CurrentAnimationState currAnimState;

    // Start is called before the first frame update
    void Awake()
    {
        isToggled = false;
        currProjectileVelocity = minProjectileVelocity;

        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        UpdateCurrentAnimationState();
    }

    public void RecieveHeldClickTime(float deltaTime) { }
    public bool IsInUse() { return false; }

    public void PlayAnimation(EquipmentAnimationTrigger trigger) 
    {
        switch (trigger)
        {
            case EquipmentAnimationTrigger.unholster:
                rend.enabled = true;
                anim.SetTrigger("Unholster");
                break;
            case EquipmentAnimationTrigger.holster:
                anim.SetTrigger("Holster");
                setToDeactivate = true;
                break;
            case EquipmentAnimationTrigger.swing:
                isInUse = true;
                anim.SetTrigger("Fire");
                break;
            case EquipmentAnimationTrigger.quickSwing:
                isInUse = true;
                anim.SetTrigger("Quick Fire");
                break;
            case EquipmentAnimationTrigger.hold:
                anim.SetTrigger("Cast");
                break;
        }
    }

    public void TriggerEndOfHolsterPoint()
    {
        if(setToDeactivate)
        {
            rend.enabled = false;
            setToDeactivate = false;
        }
    }

    public bool IsCompletelyHolstered()
    {
        return rend.enabled;
    }

    public void Fire()
    {
        doneFiring = true;
        GameObject newProjectile  = Instantiate(projectilePrefab, projectileSpawn.position, Quaternion.identity);

        newProjectile.GetComponent<Rigidbody>().velocity = projectileSpawn.TransformDirection(Vector3.left * currProjectileVelocity);

        currProjectileVelocity = minProjectileVelocity;
        Destroy(newProjectile, 10f);
    }

    public void HoldingButton() 
    {
        if (currProjectileVelocity < maxProjectileVelocity)
            currProjectileVelocity += Time.deltaTime * 4.5f;

        if (!isHolding)
        {
            isHolding = true;
            PlayAnimation(EquipmentAnimationTrigger.hold);
        }
    }

    public void ReleaseHold() 
    {
        isHolding = false;
        PlayAnimation(EquipmentAnimationTrigger.swing);
    }
    public void UseRightAway() 
    {
        PlayAnimation(EquipmentAnimationTrigger.quickSwing);
    }

    public void ToggleEquipment()
    {
        isToggled = !isToggled;

        if (isToggled) PlayAnimation(EquipmentAnimationTrigger.unholster);
        else PlayAnimation(EquipmentAnimationTrigger.holster);
    }


    public bool GetToggle() 
    { 
        return isToggled; 
    }

    public void FinishedFiring()
    {
        doneFiring = true;
    }

    public bool HasFinishedFiring()
    {
        return doneFiring;
    }

    public bool HasSuccesfullyFired()
    {
        return doneFiring;
    }

    public void ResetExternalTriggers()
    {
        //Nothing
    }

    private void UpdateCurrentAnimationState()
    {
        if (anim.GetCurrentAnimatorStateInfo(0).IsName("Idle")) currAnimState = CurrentAnimationState.Idle;
        else if (anim.GetCurrentAnimatorStateInfo(0).IsName("Unholstering")) currAnimState = CurrentAnimationState.Unholstering;
        else if (anim.GetCurrentAnimatorStateInfo(0).IsName("Holstering")) currAnimState = CurrentAnimationState.Holstering;
        else if (anim.GetCurrentAnimatorStateInfo(0).IsName("Casting")) currAnimState = CurrentAnimationState.Casting;
        else if (anim.GetCurrentAnimatorStateInfo(0).IsName("Firing")) currAnimState = CurrentAnimationState.Firing;
        else if (anim.GetCurrentAnimatorStateInfo(0).IsName("Retracting")) currAnimState = CurrentAnimationState.Retracting;
    }

    public CurrentAnimationState GetCurrentAnimationState()
    {
        return currAnimState;
    }
}
