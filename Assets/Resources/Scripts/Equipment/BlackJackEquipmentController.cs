﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlackJackEquipmentController : MonoBehaviour, IEquipment
{
    bool isHolding;
    bool isToggled;
    bool isInUse;
    public Animator anim;
    public Renderer rend;

    bool setToDeactivate;

    public CurrentAnimationState currAnimState;

    public float dmgOutput = 20f;

    // Start is called before the first frame update
    void Awake()
    {
        isToggled = false;

        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        //TODO: Get rid of this...
        if(isInUse && anim.GetCurrentAnimatorStateInfo(0).IsName("Idle"))
        {
            isInUse = false;
        }
    }


    public void RecieveHeldClickTime(float deltaTime)
    {
        
    }

    public bool IsInUse()
    {
        return isInUse;
    }

    public void PlayAnimation(EquipmentAnimationTrigger trigger)
    {
        switch(trigger)
        {
            case EquipmentAnimationTrigger.unholster:
                rend.enabled = true;
                anim.SetTrigger("Unholster");
                break;
            case EquipmentAnimationTrigger.holster:
                anim.SetTrigger("Holster");
                setToDeactivate = true;
                break;
            case EquipmentAnimationTrigger.swing:
                isInUse = true;
                anim.SetTrigger("Swing");
                break;
            case EquipmentAnimationTrigger.quickSwing:
                isInUse = true;
                anim.SetTrigger("Quick Swing");
                break;
            case EquipmentAnimationTrigger.hold:
                anim.SetTrigger("Hold");
                break;
        }
    }

    public void TriggerEndOfHolsterPoint()
    {
        if(setToDeactivate)
        {
            rend.enabled = false;
            setToDeactivate = false;
        }
    }

    public bool IsCompletelyHolstered()
    {
        return rend.enabled;
    }

    public void HoldingButton()
    {
        if (!isHolding)
        {
            isHolding = true;
            PlayAnimation(EquipmentAnimationTrigger.hold);
        }
    }

    public void ReleaseHold()
    {
        isHolding = false;
        PlayAnimation(EquipmentAnimationTrigger.swing);
    }
    public void UseRightAway()
    {
        PlayAnimation(EquipmentAnimationTrigger.quickSwing);
    }

    public void ToggleEquipment()
    {
        isToggled = !isToggled;

        if(isToggled) PlayAnimation(EquipmentAnimationTrigger.unholster);
        else PlayAnimation(EquipmentAnimationTrigger.holster);
    }

    public bool GetToggle()
    {
        return isToggled;
    }

    public void ResetAttackingAnimationTriggers()
    {
        print("resetting");
        anim.ResetTrigger("Quick Swing");
        anim.ResetTrigger("Swing");
        anim.ResetTrigger("Hold");
    }

    public bool HasFinishedFiring()
    {
        return false;
    }

    public bool HasSuccesfullyFired()
    {
        return false;
    }

    public void ResetExternalTriggers()
    {
        //Nothing
    }

    private void UpdateCurrentAnimationState()
    {
        if (anim.GetCurrentAnimatorStateInfo(0).IsName("Idle")) currAnimState = CurrentAnimationState.Idle;
        else if (anim.GetCurrentAnimatorStateInfo(0).IsName("Unholstering")) currAnimState = CurrentAnimationState.Unholstering;
        else if (anim.GetCurrentAnimatorStateInfo(0).IsName("Holstering")) currAnimState = CurrentAnimationState.Holstering;
        else if (anim.GetCurrentAnimatorStateInfo(0).IsName("Casting")) currAnimState = CurrentAnimationState.Casting;
        else if (anim.GetCurrentAnimatorStateInfo(0).IsName("Firing")) currAnimState = CurrentAnimationState.Firing;
        else if (anim.GetCurrentAnimatorStateInfo(0).IsName("Retracting")) currAnimState = CurrentAnimationState.Retracting;
    }

    public CurrentAnimationState GetCurrentAnimationState()
    {
        return currAnimState;
    }

    public void OnTriggerEnter(Collider other)
    {
        if(other.tag == "EnemyWeakSpot")
        {
            other.gameObject.GetComponent<EnemyAIWeakSpot>().KnockOut();
        }
        if(other.tag == "Enemy")
        {
            other.gameObject.GetComponent<EnemyAIStats>().ChangeHealthByIncrememnt(-dmgOutput);
        }
    }
}

