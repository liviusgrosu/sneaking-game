﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SilencerActiveSpell : MonoBehaviour, ISpells
{
    private int SpellId;

    public float maxDuration;
    private float currDuration;

    private PlayerStats stats;

    public float newSoundMod = 0.1f;

    //OnCreate function 
    // - Starts down the countdown timer 
    // - Once it reaches 0 then it needs to be destroyed

    public void OnCreate(int id)
    {
        SpellId = id;

        currDuration = maxDuration;
        stats = transform.parent.GetComponent<PlayerStats>();

        stats.ChangeSoundMod(newSoundMod);
    }


    private void Update()
    {
        currDuration -= Time.deltaTime;
        if (currDuration <= 0f)
            DestroyOnDuration();
    }


    public void OnHit()
    {
         
    }

    private void DestroyOnDuration()
    {
        stats.ResetSoundMod();
        stats.RemoveSpellFromList(SpellId);
    }

    private void DestroyOnImpact()
    {
        stats.ResetSoundMod();
    }

    public int GetId()
    {
        return SpellId;
    }
}
