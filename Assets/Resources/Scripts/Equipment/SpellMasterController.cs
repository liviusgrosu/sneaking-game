﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpellMasterController : MonoBehaviour, IEquipment
{
    public void RecieveHeldClickTime(float deltaTime)
    {
        this.gameObject.GetComponent<IEquipment>().RecieveHeldClickTime(deltaTime);
    }
    public bool IsInUse()
    {
        return this.gameObject.GetComponent<IEquipment>().IsInUse();
    }

    public void PlayAnimation(EquipmentAnimationTrigger trigger)
    {
        this.gameObject.GetComponent<IEquipment>().PlayAnimation(trigger);
    }

    public bool IsCompletelyHolstered()
    {
        return this.gameObject.GetComponent<IEquipment>().IsCompletelyHolstered();
    }

    public void HoldingButton()
    {
        this.gameObject.GetComponent<IEquipment>().HoldingButton();
    }
    public void ReleaseHold()
    {
        this.gameObject.GetComponent<IEquipment>().ReleaseHold();
    }
    public void UseRightAway()
    {
        this.gameObject.GetComponent<IEquipment>().UseRightAway();
    }

    public bool GetToggle()
    {
        return this.gameObject.GetComponent<IEquipment>().GetToggle();
    }
    public void ToggleEquipment()
    {
        this.gameObject.GetComponent<IEquipment>().ToggleEquipment();
    }

    public CurrentAnimationState GetCurrentAnimationState()
    {
        return this.gameObject.GetComponent<IEquipment>().GetCurrentAnimationState();
    }
    public bool HasFinishedFiring()
    {
        return this.gameObject.GetComponent<IEquipment>().HasFinishedFiring();
    }

    public bool HasSuccesfullyFired()
    {
        return this.gameObject.GetComponent<IEquipment>().HasSuccesfullyFired();
    }

    public void ResetExternalTriggers()
    {
        this.gameObject.GetComponent<IEquipment>().ResetExternalTriggers();
    }
}
